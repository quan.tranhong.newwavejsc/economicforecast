import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/ui/pages/dialog/item/index/index_cubit.dart';

class IndexItem extends StatefulWidget {
  ChartPresenter? chartPresenter;
  bool? isSelected;
  Function selectItemListener;

  IndexItem(
      {Key? key,
      this.chartPresenter,
      required this.selectItemListener,
      this.isSelected})
      : super(key: key);

  @override
  IndexItemState createState() => IndexItemState();
}

class IndexItemState extends State<IndexItem> {
  late bool _isSelected;
  late IndexCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<IndexCubit>(context);
    _isSelected = widget.isSelected ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (!_isSelected) {
          _isSelected = true;
          _cubit.update(select: _isSelected);
          widget.selectItemListener(
              chartPresenter: widget.chartPresenter, isSelected: _isSelected);
        }
      },
      child: BlocBuilder<IndexCubit, IndexState>(
          buildWhen: (prev, current) => prev.select != current.select,
          builder: (context, state) {
            return Container(
              decoration: BoxDecoration(
                  color: !(state.select ?? _isSelected)
                      ? Colors.white
                      : AppColors.greenForecastHex,
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: AppShadow.boxLightShadow),
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.symmetric(vertical: 3),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    width: 40,
                    height: 20,
                    decoration: BoxDecoration(
                      color: AppColors.chartColors[widget.chartPresenter!.key!],
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                  Container(height: 10),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        widget.chartPresenter?.name ?? "",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: /*_isSelected
                                ? Colors.white
                                : */AppColors.backgroundBlueDark),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
    );
  }

  void updateChoiceMatter({required bool select}) {
    _isSelected = select;
    _cubit.update(select: _isSelected);
  }
}
