import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_images.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';

class AccountItem extends StatefulWidget {
  ForecastDurationType? type;
  bool? isSelected;
  Function selectItemListener;

  AccountItem(
      {Key? key, this.type, required this.selectItemListener, this.isSelected})
      : super(key: key);

  @override
  AccountItemState createState() => AccountItemState();
}

class AccountItemState extends State<AccountItem> {
  late bool _isSelected;

  @override
  void initState() {
    super.initState();
    _isSelected = widget.isSelected ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (!_isSelected) {
            _isSelected = true;
            widget.selectItemListener(
                type: widget.type, isSelected: _isSelected);
          }
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: !_isSelected ? AppColors.whiteLightE8 : AppColors.backgroundBlueDark.withOpacity(0.7),
            borderRadius: BorderRadius.circular(12),
            boxShadow: AppShadow.boxLightE8Shadow),
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: (ScreenSize.of(context).width * 0.8 / 2) - 20,
              height: (ScreenSize.of(context).width * 0.2 / 2) - 40,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
              ),
              child: SizedBox.fromSize(
                child: FittedBox(
                  child: Image.asset(
                      widget.type == ForecastDurationType.SIX_MONTH
                          ? AppImages.icSixMonth
                          : AppImages.icAYear),
                ),
              ), // You can add a Icon instead of text also, like below.
            ),
            Container(height: 10),
            Container(
              padding: EdgeInsets.all(5),
              child: Center(
                child: Text(
                  widget.type?.name ?? "",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: _isSelected
                          ? Colors.white
                          : AppColors.backgroundBlueDark),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void updateChoiceMatter() {
    setState(() {
      if (_isSelected) {
        _isSelected = false;
      } else {
        _isSelected = true;
      }
    });
  }
}
