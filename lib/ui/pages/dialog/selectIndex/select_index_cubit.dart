import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:meta/meta.dart';

part 'select_index_state.dart';

class SelectIndexCubit extends Cubit<SelectIndexState> {
  SelectIndexCubit() : super(SelectIndexState());

  /// quanth: statistical
  void updatePresenter({ChartPresenter? presenter}) async {
    emit(state.copyWith(
      presenterStatus: LoadStatus.LOADING,
    ));
    try {
      emit(
        state.copyWith(
          presenter: presenter,
          presenterStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(presenterStatus: LoadStatus.FAILURE));
    }
  }
}
