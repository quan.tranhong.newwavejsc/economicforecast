import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/dialog/item/index/index_cubit.dart';
import 'package:flutter_base/ui/pages/dialog/item/index/index_item.dart';
import 'package:flutter_base/ui/pages/dialog/selectIndex/select_index_cubit.dart';

class SelectIndexDialog {
  final BuildContext context;
  List<ChartPresenter>? chartPresenters;
  Function itemSelected;

  SelectIndexDialog({
    required this.context,
    required this.chartPresenters,
    required this.itemSelected,
  }) : assert(context != null);

  void _selectItemListener({required ChartPresenter chartPresenter}) {
    itemSelected(chartPresenter: chartPresenter);
    Navigator.of(context).pop();
  }

  void show() {
    showDialog(
        useRootNavigator: true,
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return BlocProvider(
            create: (context) {
              return SelectIndexCubit();
            },
            child: DialogContentPage(
              chartPresenters: chartPresenters,
              selectItemListener: _selectItemListener,
            ),
          );
        });
  }
}

class DialogContentPage extends StatefulWidget {
  List<ChartPresenter>? chartPresenters;
  Function selectItemListener;

  DialogContentPage({
    Key? key,
    this.chartPresenters,
    required this.selectItemListener,
  }) : super(key: key);

  @override
  State<DialogContentPage> createState() => DialogContentState();
}

class DialogContentState extends State<DialogContentPage> {
  List<GlobalKey<IndexItemState>> itemGlobalKeys = [];

  late SelectIndexCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<SelectIndexCubit>(context);
    _cubit.updatePresenter(presenter: widget.chartPresenters![0]);
  }

  void _selectItemListener(
      {required ChartPresenter chartPresenter, required bool isSelected}) {
    _cubit.updatePresenter(presenter: chartPresenter);
    int index = widget.chartPresenters?.indexOf(chartPresenter) ?? -1;
    for (int i = 0; i < itemGlobalKeys.length; i++) {
      if (index != -1 && index != i) {
        itemGlobalKeys[i].currentState?.updateChoiceMatter(select: false);
      }
    }
  }

  void _closeClickedListener() {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> items = [];
    itemGlobalKeys = [];
    // lưu lại danh sách key
    for (int i = 0; i < (widget.chartPresenters?.length ?? 0); i++) {
      GlobalKey<IndexItemState> key = GlobalKey();
      itemGlobalKeys.add(key);
      items.add(
        BlocProvider(
          create: (context) {
            return IndexCubit();
          },
          child: IndexItem(
              key: key,
              isSelected: i == 0,
              chartPresenter: widget.chartPresenters![i],
              selectItemListener: _selectItemListener),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        color: Colors.transparent,
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Container(
                width: ScreenSize.of(context).width * 0.8,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      height: 50,
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 5,
                            ),
                            child: Text(
                              "Lựa chọn chỉ tiêu dự báo",
                              style: AppTextStyle.blueDarkS15Bold,
                            ),
                          ),
                          Spacer(),
                          CircleButton(
                            icon: Icon(
                              Icons.close,
                              color: AppColors.backgroundBlueDark,
                            ),
                            onTapListenter: _closeClickedListener,
                          ),
                        ],
                      ),
                    ),
                    Container(height: 10),
                    Column(
                      children: items,
                    ),
                    Container(height: 10),
                    BlocBuilder<SelectIndexCubit, SelectIndexState>(
                        buildWhen: (prev, current) =>
                        prev.presenterStatus != current.presenterStatus,
                        builder: (context, state) {
                          if (state.presenterStatus == LoadStatus.LOADING) {
                            return Container();
                          } else if (state.presenterStatus == LoadStatus.FAILURE) {
                            return Container();
                          }
                          if(!(state.presenter?.key == 0))
                            return Container(
                              height: 60,
                              child: Column(
                                children: [
                                  Text(
                                    "Hiện tại sản phẩm chỉ hỗ trợ dự báo cho \"Chỉ số giá tiêu dùng chung\". Chúng tôi sẽ cập nhật thêm trong thời gian tới.",
                                    style: AppTextStyle.redS12,
                                  ),
                                  Container(height: 10),
                                ],
                              ),
                            );
                          else
                            return Container(height: 20);
                        }),
                    BlocBuilder<SelectIndexCubit, SelectIndexState>(
                        buildWhen: (prev, current) =>
                        prev.presenterStatus != current.presenterStatus,
                        builder: (context, state) {
                          if (state.presenterStatus == LoadStatus.LOADING) {
                            return Container();
                          } else if (state.presenterStatus == LoadStatus.FAILURE) {
                            return Container();
                          }
                          return GestureDetector(
                            onTap: () {
                              if(state.presenter?.key == 0)
                                widget.selectItemListener(
                                    chartPresenter: _cubit.state.presenter);
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: (!(state.presenter?.key == 0)) ? AppColors.gray :AppColors.backgroundBlueDark,
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                boxShadow: AppShadow.boxShadow,
                              ),
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Center(
                                child: Text(
                                  "Áp dụng",
                                  style: AppTextStyle.whiteS16Bold,
                                ),
                              ),
                            ),
                          );
                        }),
                    Container(height: 10),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
