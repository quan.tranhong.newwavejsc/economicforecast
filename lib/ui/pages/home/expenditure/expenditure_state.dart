part of 'expenditure_cubit.dart';

@immutable
class ExpState extends Equatable {
  final ExpenditureEntity? expEntity;
  final ExForecastEntity? expForecastEntity;
  final LoadStatus? fetchRevexStatus;

  /// quanth: forecast
  final LoadStatus? forecastFilterStatus;
  LineChartFilterParam? forecastFilterParams;
  ForecastDurationType? forecastType;
  final LoadStatus? forecastLBStatus;
  List<LineBarSpot>? forecastLBSpots = [];

  ExpState(
      {this.expEntity,
      this.expForecastEntity,
      this.fetchRevexStatus,
      this.forecastFilterStatus,
      this.forecastFilterParams,
      this.forecastType,
      this.forecastLBStatus,
      this.forecastLBSpots});

  ExpState copyWith({
    ExpenditureEntity? expEntity,
    ExForecastEntity? expForecastEntity,
    LoadStatus? fetchRevexStatus,
    LoadStatus? forecastFilterStatus,
    LineChartFilterParam? forecastFilterParams,
    ForecastDurationType? forecastType,
    LoadStatus? forecastLBStatus,
    List<LineBarSpot>? forecastLBSpots,
  }) {
    return ExpState(
      expEntity: expEntity ?? this.expEntity,
      expForecastEntity: expForecastEntity ?? this.expForecastEntity,
      fetchRevexStatus: fetchRevexStatus ?? this.fetchRevexStatus,
      forecastFilterStatus: forecastFilterStatus ?? this.forecastFilterStatus,
      forecastFilterParams: forecastFilterParams ?? this.forecastFilterParams,
      forecastType: forecastType ?? this.forecastType,
      forecastLBStatus: forecastLBStatus ?? this.forecastLBStatus,
      forecastLBSpots: forecastLBSpots ?? this.forecastLBSpots,
    );
  }

  List<ExpenditureDataDetailEntity>? get expForecastList {
    int start = 0;
    int size = 15;
    List<ExpenditureDataDetailEntity>? list = [expEntity?.data?.totalExport ?? ExpenditureDataDetailEntity()];
    int length = list.length;
    if(length <= 15) {
      start = 0;
      size = length;
    } else {
      start = length - 15;
      size = 15;
    }

    return list.sublist(start, size);
  }

  /// quanth bởi api k hỗ trợ sublist nên phải làm local code
  List<String>? get forecastTimeLines {
    List<String> list = expEntity?.data?.timeline ?? [];
    int range = Utils.checkInt(forecastType?.statValue ?? 0.0)!;
    if (list.isNotEmpty) {
      switch (forecastType) {
        case ForecastDurationType.SIX_MONTH:
          return list.sublist((list.length - 3) - range, list.length - 3);
        case ForecastDurationType.A_YEAR:
          return list.sublist((list.length - 3) - range, list.length - 3);
        case ForecastDurationType.NINE_MONTH:
          return list.sublist(list.length - range, list.length);
        case ForecastDurationType.OVER_A_YEAR:
          return list.sublist(list.length - range, list.length);
        case ForecastDurationType.ALL_YEAR:
          return list.sublist(0, list.length - 3);
        case ForecastDurationType.OVER_ALL_YEAR:
          return list.sublist(list.length - range, list.length);
        default:
          return list;
      }
    }
    return [];
  }

  List<double>? get forecastBETExpenditure {
    List<double> expenditures = expForecastEntity?.data?.export ?? [];
    return expenditures.sublist(0, 3);
  }

  List<double>? get forecastUPExpenditure {
    List<double> expenditures = expForecastEntity?.data?.upper ?? [];
    return expenditures.sublist(0, 3);
  }

  List<double>? get forecastLOWExpenditure {
    List<double> expenditures = expForecastEntity?.data?.lower ?? [];
    return expenditures.sublist(0, 3);
  }

  @override
  List<Object?> get props => [
        this.expEntity,
        this.expForecastEntity,
        this.fetchRevexStatus,
        this.forecastFilterStatus,
        this.forecastFilterParams,
        this.forecastType,
        this.forecastLBStatus,
        this.forecastLBSpots
      ];
}
