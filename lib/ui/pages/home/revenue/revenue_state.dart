part of 'revenue_cubit.dart';

@immutable
class RevState extends Equatable {
  final RevenueEntity? revEntity;
  final ImForecastEntity? revForecastEntity;
  final ExpenditureEntity? expEntity;
  final LoadStatus? fetchRevexStatus;

  /// quanth: forecast
  final LoadStatus? forecastFilterStatus;
  LineChartFilterParam? forecastFilterParams;
  ForecastDurationType? forecastType;
  final LoadStatus? forecastLBStatus;
  List<LineBarSpot>? forecastLBSpots = [];

  RevState(
      {this.revEntity,
      this.revForecastEntity,
      this.expEntity,
      this.fetchRevexStatus,
      this.forecastFilterStatus,
      this.forecastFilterParams,
      this.forecastType,
      this.forecastLBStatus,
      this.forecastLBSpots});

  RevState copyWith({
    RevenueEntity? revEntity,
    ImForecastEntity? revForecastEntity,
    ExpenditureEntity? expEntity,
    LoadStatus? fetchRevexStatus,
    LoadStatus? forecastFilterStatus,
    LineChartFilterParam? forecastFilterParams,
    ForecastDurationType? forecastType,
    LoadStatus? forecastLBStatus,
    List<LineBarSpot>? forecastLBSpots,
  }) {
    return RevState(
      revEntity: revEntity ?? this.revEntity,
      revForecastEntity: revForecastEntity ?? this.revForecastEntity,
      fetchRevexStatus: fetchRevexStatus ?? this.fetchRevexStatus,
      forecastFilterStatus: forecastFilterStatus ?? this.forecastFilterStatus,
      forecastFilterParams: forecastFilterParams ?? this.forecastFilterParams,
      forecastType: forecastType ?? this.forecastType,
      forecastLBStatus: forecastLBStatus ?? this.forecastLBStatus,
      forecastLBSpots: forecastLBSpots ?? this.forecastLBSpots,
    );
  }

  List<RevenueDataDetailEntity>? get revForecastCpies {
    int start = 0;
    int size = 15;
    List<RevenueDataDetailEntity>? list = [revEntity?.data?.totalImport ?? RevenueDataDetailEntity()];
    int length = list.length;
    if(length <= 15) {
      start = 0;
      size = length;
    } else {
      start = length - 15;
      size = 15;
    }

    return list.sublist(start, size);
  }

  /// quanth bởi api k hỗ trợ sublist nên phải làm local code
  List<String>? get forecastTimeLines {
    List<String> list = revEntity?.data?.timeline ?? [];
    int range = Utils.checkInt(forecastType?.statValue ?? 0.0)!;
    if (list.isNotEmpty) {
      switch (forecastType) {
        case ForecastDurationType.SIX_MONTH:
          return list.sublist((list.length - 3) - range, list.length - 3);
        case ForecastDurationType.A_YEAR:
          return list.sublist((list.length - 3) - range, list.length - 3);
        case ForecastDurationType.NINE_MONTH:
          return list.sublist(list.length - range, list.length);
        case ForecastDurationType.OVER_A_YEAR:
          return list.sublist(list.length - range, list.length);
        case ForecastDurationType.ALL_YEAR:
          return list.sublist(0, list.length - 3);
        case ForecastDurationType.OVER_ALL_YEAR:
          return list.sublist(list.length - range, list.length);
        default:
          return list;
      }
    }
    return [];
  }

  List<double>? get forecastBETRevenues {
    List<double> revenues = revForecastEntity?.data?.import ?? [];
    return revenues.sublist(0, 3);
  }

  List<double>? get forecastUPRevenues {
    List<double> revenues = revForecastEntity?.data?.upper ?? [];
    return revenues.sublist(0, 3);
  }

  List<double>? get forecastLOWRevenues {
    List<double> revenues = revForecastEntity?.data?.lower ?? [];
    return revenues.sublist(0, 3);
  }

  @override
  List<Object?> get props => [
        this.revEntity,
        this.revForecastEntity,
        this.fetchRevexStatus,
        this.forecastFilterStatus,
        this.forecastFilterParams,
        this.forecastType,
        this.forecastLBStatus,
        this.forecastLBSpots
      ];
}
