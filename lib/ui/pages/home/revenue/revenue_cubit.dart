import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/params/line_chart_filter_param.dart';
import 'package:flutter_base/repositories/chart_repository.dart';
import 'package:flutter_base/utils/logger.dart';
import 'package:flutter_base/utils/utils.dart';
import 'package:meta/meta.dart';
import 'package:flutter_base/utils/extensions.dart';

part 'revenue_state.dart';

class RevCubit extends Cubit<RevState> {
  ChartRepository? repository;

  RevCubit({this.repository}) : super(RevState());

  void fetchRevex() async {
    emit(state.copyWith(fetchRevexStatus: LoadStatus.LOADING));
    try {
      final revEntity = await repository?.fetchRev();
      final revForecastEntity = await repository?.fetchForecastRevenue();
      emit(state.copyWith(
          fetchRevexStatus: LoadStatus.SUCCESS, revEntity: revEntity, revForecastEntity: revForecastEntity));
    } catch (e) {
      emit(state.copyWith(fetchRevexStatus: LoadStatus.FAILURE));
      logger.e(e);
    }
  }

  /// quanth: forecast
  void filterForecastChart({List<ChartPresenter>? chartPresenters}) async {
    emit(state.copyWith(
        forecastFilterStatus: LoadStatus.LOADING,
        forecastLBStatus: LoadStatus.LOADING));
    try {
      if (state.forecastType == null) {
        emit(
          state.copyWith(
            forecastFilterStatus: LoadStatus.SUCCESS,
            forecastFilterParams:
              LineChartFilterParam(chartPresenters: chartPresenters),
            forecastLBSpots: [],
            forecastLBStatus: LoadStatus.SUCCESS,
            forecastType: ForecastDurationType.SIX_MONTH,
          ),
        );
      } else {
        /// quanth: nếu đang mở chế độ dự báo thì sẽ reset về ban đầu
        ForecastDurationType forecastType = ForecastDurationType.SIX_MONTH;
        if (state.forecastType == ForecastDurationType.NINE_MONTH) {
          forecastType = ForecastDurationType.SIX_MONTH;
        } else if (state.forecastType == ForecastDurationType.OVER_A_YEAR) {
          forecastType = ForecastDurationType.A_YEAR;
        } else {
          forecastType = state.forecastType!;
        }
        emit(
          state.copyWith(
            forecastFilterStatus: LoadStatus.SUCCESS,
            forecastFilterParams:
              LineChartFilterParam(chartPresenters: chartPresenters),
            forecastLBSpots: [],
            forecastLBStatus: LoadStatus.SUCCESS,
            forecastType: forecastType,
          ),
        );
      }
    } catch (error) {
      emit(state.copyWith(forecastFilterStatus: LoadStatus.FAILURE));
    }
  }

  void updateForecastLBSpots({List<LineBarSpot>? forecastLBSpots}) async {
    emit(state.copyWith(forecastLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          forecastLBSpots: forecastLBSpots,
          forecastLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(forecastLBStatus: LoadStatus.FAILURE));
    }
  }

  void updateForecastType(
      {ForecastDurationType? forecastType,
        ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(
      forecastLBStatus: LoadStatus.LOADING,
    ));
    try {
      LineChartFilterParam? filterParam = LineChartFilterParam(
          chartPresenters: state.forecastFilterParams?.chartPresenters,
          chartPresenter: state.forecastFilterParams?.chartPresenter,
          choosens: (chartPresenter == null) ? [] : [chartPresenter]);

      emit(
        state.copyWith(
          forecastType: forecastType,
          forecastFilterParams: filterParam,
          forecastLBSpots: [],
          forecastLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(forecastLBStatus: LoadStatus.FAILURE));
    }
  }

  void initForecastChartPresenter({ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(forecastFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          forecastFilterStatus: LoadStatus.SUCCESS,
          forecastFilterParams:
          LineChartFilterParam(chartPresenters: [chartPresenter!]),
        ),
      );
    } catch (error) {
      emit(state.copyWith(forecastFilterStatus: LoadStatus.FAILURE));
    }
  }

}
