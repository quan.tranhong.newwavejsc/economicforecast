part of 'cpi_cubit.dart';

class CpiState extends Equatable {
  final CPIEntity? cpiEntity;
  final CPIForecastEntity? cpiForecastEntity;
  final LoadStatus? fetchCPIStatus;

  /// quanth: forecast
  final LoadStatus? forecastFilterStatus;
  LineChartFilterParam? forecastFilterParams;
  ForecastDurationType? forecastType;
  final LoadStatus? forecastLBStatus;
  List<LineBarSpot>? forecastLBSpots = [];

  /// quanth: statistical
  final LoadStatus? statisticalFilterStatus;
  LineChartFilterParam? statisticalFilterParams;
  ForecastDurationType? statisticalType;
  final LoadStatus? statLBStatus;
  List<BarTouchedSpot?>? statLBSpots;

  /// quanth: compare
  final LoadStatus? compareFilterStatus;
  LineChartFilterParam? compareFilterParams;
  ForecastDurationType? compareType;
  final LoadStatus? compareLBStatus;
  List<LineBarSpot>? compareLBSpots = [];

  CpiState({
    this.cpiEntity,
    this.fetchCPIStatus,
    this.forecastFilterStatus,
    this.forecastFilterParams,
    this.forecastType,
    this.forecastLBStatus,
    this.forecastLBSpots,
    this.statisticalFilterStatus,
    this.statisticalFilterParams,
    this.statisticalType,
    this.statLBStatus,
    this.statLBSpots,
    this.compareFilterStatus,
    this.compareFilterParams,
    this.compareType,
    this.compareLBStatus,
    this.compareLBSpots,
    this.cpiForecastEntity,
  });

  CpiState copyWith({
    CPIEntity? cpiEntity,
    CPIForecastEntity? cpiForecastEntity,
    LoadStatus? fetchCPIStatus,
    LoadStatus? forecastFilterStatus,
    LineChartFilterParam? forecastFilterParams,
    ForecastDurationType? forecastType,
    LoadStatus? forecastLBStatus,
    List<LineBarSpot>? forecastLBSpots,
    LoadStatus? statisticalFilterStatus,
    LineChartFilterParam? statisticalFilterParams,
    ForecastDurationType? statisticalType,
    LoadStatus? statLBStatus,
    List<BarTouchedSpot?>? statLBSpots,
    LoadStatus? compareFilterStatus,
    LineChartFilterParam? compareFilterParams,
    ForecastDurationType? compareType,
    LoadStatus? compareLBStatus,
    List<LineBarSpot>? compareLBSpots,
  }) {
    return new CpiState(
      cpiEntity: cpiEntity ?? this.cpiEntity,
      cpiForecastEntity: cpiForecastEntity ?? this.cpiForecastEntity,
      fetchCPIStatus: fetchCPIStatus ?? this.fetchCPIStatus,
      forecastFilterStatus: forecastFilterStatus ?? this.forecastFilterStatus,
      forecastFilterParams: forecastFilterParams ?? this.forecastFilterParams,
      forecastType: forecastType ?? this.forecastType,
      forecastLBStatus: forecastLBStatus ?? this.forecastLBStatus,
      forecastLBSpots: forecastLBSpots ?? this.forecastLBSpots,
      statisticalFilterStatus:
          statisticalFilterStatus ?? this.statisticalFilterStatus,
      statisticalFilterParams:
          statisticalFilterParams ?? this.statisticalFilterParams,
      statisticalType: statisticalType ?? this.statisticalType,
      statLBStatus: statLBStatus ?? this.statLBStatus,
      statLBSpots: statLBSpots ?? this.statLBSpots,
      compareFilterStatus: compareFilterStatus ?? this.compareFilterStatus,
      compareFilterParams: compareFilterParams ?? this.compareFilterParams,
      compareType: compareType ?? this.compareType,
      compareLBStatus: compareLBStatus ?? this.compareLBStatus,
      compareLBSpots: compareLBSpots ?? this.compareLBSpots,
    );
  }


  List<CpiDataItemEntity>? get forecastCpies {
    return cpiEntity?.data?.cpi ?? [];
  }

  List<double>? get forecastBETCpies {
    List<double> cpies = cpiForecastEntity?.data?.cpi ?? [];
    return cpies.map((val) => (val + 100)).toList().sublist(0, 3);
  }

  List<double>? get forecastUPCpies {
    List<double> cpies = cpiForecastEntity?.data?.upper ?? [];
    return cpies.map((val) => (val + 100)).toList().sublist(0, 3);
  }

  List<double>? get forecastLOWCpies {
    List<double> cpies = cpiForecastEntity?.data?.lower ?? [];
    return cpies.map((val) => (val + 100)).toList().sublist(0, 3);
  }

  List<String>? get forecastTimeLines {
    List<String> list = cpiEntity?.data?.timeline ?? [];
    int range = Utils.checkInt(forecastType?.statValue ?? 0.0)!;
    if (list.isNotEmpty) {
      switch (forecastType) {
        case ForecastDurationType.SIX_MONTH:
          return list.sublist((list.length - 3) - range, list.length - 3);
        case ForecastDurationType.A_YEAR:
          return list.sublist((list.length - 3) - range, list.length - 3);
        case ForecastDurationType.NINE_MONTH:
          return list.sublist(list.length - range, list.length);
        case ForecastDurationType.OVER_A_YEAR:
          return list.sublist(list.length - range, list.length);
        case ForecastDurationType.ALL_YEAR:
          return list.sublist(0, list.length - 3);
        case ForecastDurationType.OVER_ALL_YEAR:
          return list.sublist(list.length - range, list.length);
        default:
          return list;
      }
    }
    return [];
  }

  List<CpiDataItemEntity>? get statisticalCpies {
    return cpiEntity?.data?.cpi ?? [];
  }

  List<String>? get statisticalTimeLines {
    List<String> list = cpiEntity?.data?.timeline ?? [];
    if (list.isNotEmpty) {
      switch (statisticalType) {
        case ForecastDurationType.SIX_MONTH:
          return cpiEntity?.data?.timeline?.sublist(6, 12);
        case ForecastDurationType.A_YEAR:
          return cpiEntity?.data?.timeline?.sublist(0, 12);
        case ForecastDurationType.NINE_MONTH:
          return cpiEntity?.data?.timeline?.sublist(6, 15);
        case ForecastDurationType.OVER_A_YEAR:
          return cpiEntity?.data?.timeline?.sublist(0, 15);
        default:
          return cpiEntity?.data?.timeline?.sublist(6, 12);
      }
    }
    return [];
  }

  List<CpiDataItemEntity>? get compareCpies {
    return cpiEntity?.data?.cpi ?? [];
  }

  List<String>? get compareTimeLines {
    List<String> list = cpiEntity?.data?.timeline ?? [];
    if (list.isNotEmpty) {
      switch (compareType) {
        case ForecastDurationType.SIX_MONTH:
          return cpiEntity?.data?.timeline?.sublist(6, 12);
        case ForecastDurationType.A_YEAR:
          return cpiEntity?.data?.timeline?.sublist(0, 12);
        case ForecastDurationType.NINE_MONTH:
          return cpiEntity?.data?.timeline?.sublist(6, 15);
        case ForecastDurationType.OVER_A_YEAR:
          return cpiEntity?.data?.timeline?.sublist(0, 15);
        default:
          return cpiEntity?.data?.timeline?.sublist(6, 12);
      }
    }
    return [];
  }

  @override
  List<Object> get props => [
        this.fetchCPIStatus ?? LoadStatus.LOADING,
        this.cpiEntity ?? CPIEntity(),
        this.forecastFilterStatus ?? LoadStatus.LOADING,
        this.forecastFilterParams ?? LineChartFilterParam(),
        this.statisticalFilterStatus ?? LoadStatus.LOADING,
        this.statisticalFilterParams ?? LineChartFilterParam(),
        this.compareFilterStatus ?? LoadStatus.LOADING,
        this.compareFilterParams ?? LineChartFilterParam(),
        this.forecastType ?? ForecastDurationType.SIX_MONTH,
        this.statisticalType ?? ForecastDurationType.SIX_MONTH,
        this.compareType ?? ForecastDurationType.SIX_MONTH,
        this.forecastLBSpots ?? [],
        this.forecastLBStatus ?? LoadStatus.LOADING,
        this.statLBSpots ?? [],
        this.statLBStatus ?? LoadStatus.LOADING,
        this.compareLBSpots ?? [],
        this.compareLBStatus ?? LoadStatus.LOADING,
        this.cpiForecastEntity ?? CPIForecastEntity(),
      ];
}
