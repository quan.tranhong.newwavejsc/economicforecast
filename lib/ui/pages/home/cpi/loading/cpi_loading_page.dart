import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:shimmer/shimmer.dart';

class CPILoadingPage extends StatefulWidget {
  @override
  _CPILoadingPageState createState() => _CPILoadingPageState();
}

class _CPILoadingPageState extends State<CPILoadingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundBlueLight,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildBanner(),
            _buildTopHeader(),
            _buildRoundBoxLoading(),
            _buildRoundBoxLoading(),
          ],
        ),
      ),
    );
  }

  Widget _buildRoundBoxLoading() {
    return Container(
      height: 400,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Container(
        height: 400,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12)),
        margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildLargeHeader(),
            _buildSmallHeader(),
            _buildMidSmallHeader(),
            Expanded(child: _buildChart()),
            _buildFooter()
          ],
        ),
      ),
    );
  }

  Widget _buildLargeHeader() {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[350]!,
        highlightColor: Colors.grey[100]!,
        child: Container(
          height: 30,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: AppShadow.boxShadow),
        ),
      ),
    );
  }

  Widget _buildSmallHeader() {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[350]!,
        highlightColor: Colors.grey[100]!,
        child: Container(
          width: ScreenSize.of(context).width / 2,
          height: 20,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: AppShadow.boxShadow),
        ),
      ),
    );
  }

  Widget _buildMidSmallHeader({String? text}) {
    return Center(
      child: Container(
        margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[350]!,
          highlightColor: Colors.grey[100]!,
          child: Container(
            height: 0,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                boxShadow: AppShadow.boxShadow),
          ),
        ),
      ),
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[350]!,
        highlightColor: Colors.grey[100]!,
        child: Container(
          height: 200,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: AppShadow.boxShadow),
        ),
      ),
    );
  }

  Widget _buildFooter() {
    return Center(
      child: Container(
        margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[350]!,
          highlightColor: Colors.grey[100]!,
          child: Container(
            width: ScreenSize.of(context).width / 2,
            height: 20,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                boxShadow: AppShadow.boxShadow),
          ),
        ),
      ),
    );
  }

  Widget _buildTopHeader() {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[350]!,
        highlightColor: Colors.grey[100]!,
        child: Container(
          width: ScreenSize.of(context).width / 2,
          height: 20,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: AppShadow.boxShadow),
        ),
      ),
    );
  }

  Widget _buildBanner() {
    return Container(
      margin: EdgeInsets.only(bottom: AppDimens.statusMarginHorizontal),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[350]!,
        highlightColor: Colors.grey[100]!,
        child: Container(
          width: ScreenSize.of(context).width,
          height: 200,
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: AppShadow.boxShadow),
        ),
      ),
    );
  }
}
