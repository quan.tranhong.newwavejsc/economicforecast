import 'dart:collection';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_images.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/cpi/statistical/cpi_statistical_bottom_sheet_cubit.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/cpi/statistical/cpi_statistical_bottom_sheet_page.dart';
import 'package:flutter_base/ui/pages/chart/cpi/cpi_compare_chart/cpi_compare_chart_page.dart';
import 'package:flutter_base/ui/pages/dialog/month_calendar_dialog.dart';
import 'package:flutter_base/ui/pages/home/cpi/cpi_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CPICompareRoundBoxPage extends StatefulWidget {
  String? topRightLargeText = "";
  String? topRightSmallText = "";
  String? bottomMidText = "";

  CPICompareRoundBoxPage({
    this.topRightLargeText,
    this.topRightSmallText,
    this.bottomMidText,
  });

  @override
  _CPICompareRoundBoxPageState createState() => _CPICompareRoundBoxPageState();
}

class _CPICompareRoundBoxPageState extends State<CPICompareRoundBoxPage> {
  late CpiCubit _cubit;
  List<ChartPresenter> _chartPresenters = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<CpiCubit>(context);
    createChartPresents(cpies: _cubit.state.compareCpies);
    _cubit.initCompareChartPresenter(chartPresenter: _chartPresenters[0]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<CpiCubit, CpiState>(
            buildWhen: (prev, current) =>
                prev.compareType != current.compareType,
            builder: (context, state) {
              return _buildLargeHeader(text: widget.topRightLargeText);
            },
          ),
          // _buildSmallHeader(text: widget.topRightSmallText),
          BlocBuilder<CpiCubit, CpiState>(
            buildWhen: (prev, current) =>
                prev.compareLBStatus != current.compareLBStatus,
            builder: (context, state) {
              return _buildMidSmallHeader(
                  lineBarSpots: state.compareLBSpots ?? []);
            },
          ),
          Expanded(child: _buildChart()),
          _buildDescription(),
          _buildFooter(),
        ],
      ),
    );
  }

  Widget _buildDescription() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(AppImages.icLine),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "tỉnh Đồng Nai",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(AppImages.icDash),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Cả nước",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text ?? "",
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          CircleButton(
            icon: Icon(
              Icons.calendar_today_rounded,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _calendarClickedListener,
            forecastType:
                _cubit.state.compareType ?? ForecastDurationType.SIX_MONTH,
          ),
          Container(
            width: 10,
          ),
          CircleButton(
            icon: Icon(
              Icons.settings,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _settingClickedListener,
          ),
        ],
      ),
    );
  }

  Widget _buildMidSmallHeader({List<LineBarSpot>? lineBarSpots}) {
    if (lineBarSpots!.isEmpty)
      return Container(
        height: 50,
      );
    List<Widget> list = [];
    for (int i = 0; i < lineBarSpots.length; i++) {
      list.add(_buildMidSmallItem(lineBarSpot: lineBarSpots[i], index: i));
    }

    String time = "";
    if (lineBarSpots.isNotEmpty) {
      String arr = _cubit.state.compareTimeLines![(lineBarSpots[0].x - 1).toInt()];
      String year = arr.split("-")[0];
      String month = arr.split("-")[1];
      time = "Tháng $month Năm $year";
    }

    return Center(
      child: Container(
        height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (lineBarSpots.isNotEmpty)
                ? Text(
                    time,
                    style: AppTextStyle.blueDarkS12,
                  )
                : Container(),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: list,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMidSmallItem({LineBarSpot? lineBarSpot, int? index}) {
    return Row(
      children: [
        Text(
          index == 0 ? "Tỉnh Đồng Nai:" : "Cả nước:",
          style: AppTextStyle.blueDarkS12,
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          lineBarSpot!.y.toString(),
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: BlocBuilder<CpiCubit, CpiState>(
          buildWhen: (prev, current) =>
              prev.compareFilterStatus != current.compareFilterStatus ||
              prev.compareType != current.compareType,
          builder: (context, state) {
            if (state.compareFilterStatus == LoadStatus.LOADING) {
              return Container();
            } else if (state.compareFilterStatus == LoadStatus.FAILURE) {
              return Container();
            }
            return CPICompareChartPage(
                onTouchDotListener: _onTouchDotListener,
                chartPresenter: state.compareFilterParams!.chartPresenter!,
                compareType:
                    state.compareType ?? ForecastDurationType.SIX_MONTH);
          }),
    );
  }

  Widget _buildFooter() {
    return BlocBuilder<CpiCubit, CpiState>(
        buildWhen: (prev, current) =>
            prev.compareFilterStatus != current.compareFilterStatus,
        builder: (context, state) {
          if (state.compareFilterStatus == LoadStatus.LOADING) {
            return Container();
          } else if (state.compareFilterStatus == LoadStatus.FAILURE) {
            return Container();
          }
          String text = state.compareFilterParams?.chartPresenter?.name ?? "";
          return Center(
            child: Container(
              margin: EdgeInsets.symmetric(
                  horizontal: AppDimens.statusMarginHorizontal,
                  vertical: AppDimens.statusBarHeight),
              child: Text(
                "$text ${S.of(context).bottom_title_compare}",
                style: AppTextStyle.blueDarkS12Bold,
                textAlign: TextAlign.center,
              ),
            ),
          );
        });
  }

  void _onTouchDotListener({List<LineBarSpot>? lineBarSpots}) {
    try {
      _cubit.updateCompareLBSpots(compareLBSpots: lineBarSpots);
    } catch (e, s) {
      print(s);
    }
  }

  void _onSettingClickListener() {
    // if(item.priorityId.compareTo("")!=0){
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Wrap(children: <Widget>[
              BlocProvider(
                create: (context) {
                  return CPIStatBottomSheetCubit();
                },
                child: _buildBottomSheet(),
              ),
            ]),
          );
        });
    // }
  }

  Widget _buildBottomSheet() {
    return CPIStatisBottomSheetPage(
      cpies: _cubit.state.compareCpies,
      selectItemListener: _selectItemListener,
      selectedPresenter: _cubit.state.compareFilterParams?.chartPresenter,
    );
  }

  void _selectItemListener({ChartPresenter? chartPresenter}) {
    _cubit.filterCompareChart(chartPresenter: chartPresenter);
  }

  void createChartPresents({List<CpiDataItemEntity>? cpies}) {
    _chartPresenters = [];
    for (int i = 0; i < cpies!.length; i++) {
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: cpies[i].val,
        name: cpies[i].name,
      );
      _chartPresenters.add(chartPresenter);
    }
  }

  void _calendarClickedListener() {
    MonthCalendarDialog(
      context: context,
      selectedType: _cubit.state.compareType ?? ForecastDurationType.SIX_MONTH,
      itemSelected: _itemSelected, canShowAll: false,
    ).show();
  }

  void _itemSelected({required ForecastDurationType type}) {
    _cubit.updateCompareType(compareType: type);
  }

  void _settingClickedListener() {
    _onSettingClickListener();
  }
}
