import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/entities/pie_chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/params/twice_chart_filter_param.dart';
import 'package:flutter_base/repositories/chart_repository.dart';
import 'package:flutter_base/utils/logger.dart';
import 'package:meta/meta.dart';

part 'imex_state.dart';

class ImexCubit extends Cubit<ImexState> {
  ChartRepository? repository;

  ImexCubit({this.repository}) : super(ImexState());

  void fetchImex() async {
    emit(state.copyWith(fetchImexStatus: LoadStatus.LOADING));
    try {
      final imexEntity = await repository?.fetchImEx();
      emit(state.copyWith(
          fetchImexStatus: LoadStatus.SUCCESS, imexEntity: imexEntity));
    } catch (e) {
      emit(state.copyWith(fetchImexStatus: LoadStatus.FAILURE));
      logger.e(e);
    }
  }

  /// quanth: statistical
  void filterStatisticalChart(
      {ChartPresenter? imChartPresenter,
      ChartPresenter? exChartPresenter,
      ChartPresenter? balChartPresenter}) async {
    emit(state.copyWith(
        statisticalFilterStatus: LoadStatus.LOADING,
        statLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
            statisticalFilterStatus: LoadStatus.SUCCESS,
            statisticalFilterParams: TwiceChartFilterParam(
                imChartPresenter: imChartPresenter,
                exChartPresenter: exChartPresenter,
                balChartPresenter: balChartPresenter),
            statLBSpots: [],
            statLBStatus: LoadStatus.SUCCESS),
      );
    } catch (error) {
      emit(state.copyWith(statisticalFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initStatisticalChartPresenter(
      {ChartPresenter? imChartPresenter,
      ChartPresenter? exChartPresenter,
      ChartPresenter? balChartPresenter}) async {
    emit(state.copyWith(statisticalFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          statisticalFilterStatus: LoadStatus.SUCCESS,
          statisticalFilterParams: TwiceChartFilterParam(
              imChartPresenter: imChartPresenter,
              exChartPresenter: exChartPresenter,
              balChartPresenter: balChartPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(statisticalFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initIMChartPresenter(
      {List<PieChartPresenter>? imChartPresenters}) async {
    emit(state.copyWith(imChartFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          imChartFilterStatus: LoadStatus.SUCCESS,
          imChartPresenters: imChartPresenters,
        ),
      );
    } catch (error) {
      emit(state.copyWith(imChartFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initEXChartPresenter(
      {List<PieChartPresenter>? exChartPresenters}) async {
    emit(state.copyWith(exChartFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          exChartFilterStatus: LoadStatus.SUCCESS,
          exChartPresenters: exChartPresenters,
        ),
      );
    } catch (error) {
      emit(state.copyWith(exChartFilterStatus: LoadStatus.FAILURE));
    }
  }

  void updateStatisticalType({ForecastDurationType? statisticalType}) async {
    emit(
      state.copyWith(
        statisticalType: statisticalType,
      ),
    );
  }

  void updateStatLBSpots({List<BarTouchedSpot?>? statLBSpots}) async {
    emit(state.copyWith(statLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          statLBSpots: statLBSpots,
          statLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(statLBStatus: LoadStatus.FAILURE));
    }
  }
}
