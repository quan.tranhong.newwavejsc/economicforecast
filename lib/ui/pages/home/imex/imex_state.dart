part of 'imex_cubit.dart';

@immutable
class ImexState extends Equatable {
  final ImexEntity? imexEntity;
  final LoadStatus? fetchImexStatus;

  /// quanth: statistical
  final LoadStatus? statisticalFilterStatus;
  TwiceChartFilterParam? statisticalFilterParams;
  ForecastDurationType? statisticalType;
  final LoadStatus? statLBStatus;
  List<BarTouchedSpot?>? statLBSpots;

  /// quanth: import
  final LoadStatus? imChartFilterStatus;
  List<PieChartPresenter>? imChartPresenters;

  /// quanth: export
  final LoadStatus? exChartFilterStatus;
  List<PieChartPresenter>? exChartPresenters;

  ImexState(
      {this.imexEntity,
      this.fetchImexStatus,
      this.statisticalFilterStatus,
      this.statisticalFilterParams,
      this.statisticalType,
      this.statLBStatus,
      this.statLBSpots,
      this.imChartFilterStatus,
      this.imChartPresenters,
      this.exChartFilterStatus,
      this.exChartPresenters});

  ImexState copyWith({
    ImexEntity? imexEntity,
    LoadStatus? fetchImexStatus,
    LoadStatus? statisticalFilterStatus,
    TwiceChartFilterParam? statisticalFilterParams,
    ForecastDurationType? statisticalType,
    LoadStatus? statLBStatus,
    List<BarTouchedSpot?>? statLBSpots,
    LoadStatus? imChartFilterStatus,
    List<PieChartPresenter>? imChartPresenters,
    LoadStatus? exChartFilterStatus,
    List<PieChartPresenter>? exChartPresenters,
  }) {
    return new ImexState(
      imexEntity: imexEntity ?? this.imexEntity,
      fetchImexStatus: fetchImexStatus ?? this.fetchImexStatus,
      statisticalFilterStatus:
          statisticalFilterStatus ?? this.statisticalFilterStatus,
      statisticalFilterParams:
          statisticalFilterParams ?? this.statisticalFilterParams,
      statisticalType: statisticalType ?? this.statisticalType,
      statLBStatus: statLBStatus ?? this.statLBStatus,
      statLBSpots: statLBSpots ?? this.statLBSpots,
      imChartFilterStatus: imChartFilterStatus ?? this.imChartFilterStatus,
      imChartPresenters: imChartPresenters ?? this.imChartPresenters,
      exChartFilterStatus: exChartFilterStatus ?? this.exChartFilterStatus,
      exChartPresenters: exChartPresenters ?? this.exChartPresenters,
    );
  }

  List<String>? get statisticalTimeLines {
    List<String> list = imexEntity?.data?.timeline ?? [];
    if(list.isNotEmpty){
      switch(statisticalType){
        case ForecastDurationType.SIX_MONTH:
          return imexEntity?.data?.timeline?.sublist(6,12);
        case ForecastDurationType.A_YEAR:
          return imexEntity?.data?.timeline?.sublist(0,12);
        case ForecastDurationType.NINE_MONTH:
          return imexEntity?.data?.timeline?.sublist(6,15);
        case ForecastDurationType.OVER_A_YEAR:
          return imexEntity?.data?.timeline?.sublist(0,15);
        default:
          return imexEntity?.data?.timeline?.sublist(6,12);
      }
    }
    return [];
  }

  @override
  List<Object?> get props => [
        this.imexEntity,
        this.fetchImexStatus,
        this.statisticalFilterStatus,
        this.statisticalFilterParams,
        this.imChartFilterStatus,
        this.imChartPresenters,
        this.exChartFilterStatus,
        this.exChartPresenters,
        this.statisticalType,
        this.statLBStatus,
        this.statLBSpots,
      ];
}
