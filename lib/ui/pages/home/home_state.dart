part of 'home_cubit.dart';

class HomeState extends Equatable {
  final PageType? currentPage;
  final LoadStatus? signOutStatus;

  HomeState({
    this.currentPage = PageType.CPI,
    this.signOutStatus = LoadStatus.INITIAL,
  });

  HomeState copyWith({
    PageType? page,
    LoadStatus? signOutStatus
  }) {
    return HomeState(
      currentPage: page ?? this.currentPage,
      signOutStatus: signOutStatus ?? this.signOutStatus,
    );
  }

  @override
  List<Object> get props => [this.currentPage ?? PageType.CPI, signOutStatus ?? LoadStatus.LOADING];
}
