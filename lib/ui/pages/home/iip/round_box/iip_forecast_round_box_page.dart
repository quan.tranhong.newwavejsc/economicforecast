import 'dart:collection';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/iip/forecast/iip_forecast_bottom_sheet_cubit.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/iip/forecast/iip_forecast_bottom_sheet_page.dart';
import 'package:flutter_base/ui/pages/chart/iip/iip_forecast_chart/iip_forecast_chart_page.dart';
import 'package:flutter_base/ui/pages/dialog/month_calendar_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_base/ui/pages/dialog/selectIndex/select_index_dialog.dart';
import 'package:flutter_base/utils/utils.dart';

import '../iip_cubit.dart';

class IIPForecastRoundBoxPage extends StatefulWidget {
  String? topRightLargeText = "";
  String? topRightSmallText = "";
  String? bottomMidText = "";

  IIPForecastRoundBoxPage({
    this.topRightLargeText,
    this.topRightSmallText,
    this.bottomMidText,
  });

  @override
  _IIPForecastRoundBoxPageState createState() =>
      _IIPForecastRoundBoxPageState();
}

class _IIPForecastRoundBoxPageState extends State<IIPForecastRoundBoxPage> {
  late IipCubit _cubit;
  List<ChartPresenter> _chartPresenters = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<IipCubit>(context);
    createChartPresents(iips: _cubit.state.forecastIips);
    _cubit.filterForecastChart(chartPresenters: _chartPresenters);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Stack(
        children: [
          BlocBuilder<IipCubit, IipState>(
              buildWhen: (prev, current) =>
                  prev.forecastType != current.forecastType,
              builder: (context, state) {
                if (state.forecastFilterStatus == LoadStatus.LOADING) {
                  return Container();
                } else if (state.forecastFilterStatus == LoadStatus.FAILURE) {
                  return Container();
                }
                return Visibility(
                  visible: state.forecastType ==
                          ForecastDurationType.NINE_MONTH ||
                      state.forecastType == ForecastDurationType.OVER_A_YEAR,
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      margin: EdgeInsets.only(top: 80, right: 10),
                      color: Colors.transparent/*AppColors.greenForecast*/,
                      width: state.forecastType == ForecastDurationType.NINE_MONTH
                          ? ScreenSize.of(context).width / 3.5
                          : ScreenSize.of(context).width / 5.2,
                      height: 240,
                      child: Container(
                        margin: EdgeInsets.only(top: 30),
                        /*child: Text(
                          "Dự báo",
                          style: AppTextStyle.greenDarkS12Bold,
                          textAlign: TextAlign.center,
                        ),*/
                      ),
                    ),
                  ),
                );
              }),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BlocBuilder<IipCubit, IipState>(
                buildWhen: (prev, current) =>
                    prev.forecastType != current.forecastType,
                builder: (context, state) {
                  return _buildLargeHeader(text: widget.topRightLargeText);
                },
              ),
              // _buildSmallHeader(text: widget.topRightSmallText),
              BlocBuilder<IipCubit, IipState>(
                buildWhen: (prev, current) =>
                    prev.forecastLBStatus != current.forecastLBStatus,
                builder: (context, state) {
                  if (Utils.isEnableForecastMode(_cubit.state.forecastType))
                    return _buildMidForecastSmallHeader(
                        lineBarSpots: state.forecastLBSpots ?? []);
                  else
                    return _buildMidSmallHeader(
                        lineBarSpots: state.forecastLBSpots ?? []);
                },
              ),
              Expanded(child: _buildChart()),
              _buildFooter(text: widget.bottomMidText),
              _buildForecastButton(),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text ?? "",
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          CircleButton(
            icon: Icon(
              Icons.calendar_today_rounded,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _calendarClickedListener,
            forecastType:
                _cubit.state.forecastType ?? ForecastDurationType.SIX_MONTH,
          ),
          Container(
            width: 10,
          ),
          CircleButton(
            icon: Icon(
              Icons.settings,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _settingClickedListener,
          ),
        ],
      ),
    );
  }

  Widget _buildSmallHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Text(
        text ?? "",
        style: AppTextStyle.greyS9,
      ),
    );
  }

  Widget _buildMidSmallHeader({List<LineBarSpot>? lineBarSpots}) {
    if (lineBarSpots?.isEmpty ?? false)
      return Container(
        height: 50,
      );
    List<Widget> list = [];
    for (int i = 0; i < (lineBarSpots?.length ?? 0); i++) {
      list.add(_buildMidSmallItem(lineBarSpot: lineBarSpots![i]));
    }

    String time = "";
    if (lineBarSpots?.isNotEmpty ?? false) {
      String arr = _cubit.state.forecastTimeLines![(lineBarSpots![0].x - 1).toInt()];
      String year = arr.split("-")[0];
      String month = arr.split("-")[1];
      time = "Tháng $month Năm $year";
    }

    return Center(
      child: Container(
        height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (lineBarSpots?.isNotEmpty ?? false)
                ? Text(
                    time,
                    style: AppTextStyle.blueDarkS12,
                  )
                : Container(),
            Container(
              margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: list,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMidSmallItem({LineBarSpot? lineBarSpot}) {
    return Row(
      children: [
        Container(
          width: 20,
          height: 10,
          color: lineBarSpot?.bar.colors[0],
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          (lineBarSpot?.y ?? 0).toString(),
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: BlocBuilder<IipCubit, IipState>(
          buildWhen: (prev, current) =>
              prev.forecastFilterStatus != current.forecastFilterStatus ||
              prev.forecastType != current.forecastType,
          builder: (context, state) {
            if (state.forecastFilterStatus == LoadStatus.LOADING) {
              return Container();
            } else if (state.forecastFilterStatus == LoadStatus.FAILURE) {
              return Container();
            }
            return IIPForecastChartPage(
              onTouchDotListener: _onTouchDotListener,
              timeline: _cubit.state.forecastTimeLines ?? [],
              chartPresenters:
                  state.forecastFilterParams?.chartPresenters ?? [],
              forecastType:
                  state.forecastType ?? ForecastDurationType.SIX_MONTH,
            );
          }),
    );
  }

  Widget _buildFooter({String? text}) {
    return Center(
      child: Container(
        margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
        child: Text(
          text ?? "",
          style: AppTextStyle.blueDarkS12Bold,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget _buildForecastButton() {
    return GestureDetector(
      onTap: () {
        _showSelectIndexDialog();
      },
      child: BlocBuilder<IipCubit, IipState>(
          buildWhen: (prev, current) =>
              prev.forecastType != current.forecastType,
          builder: (context, state) {
            return Center(
              child: Container(
                decoration: BoxDecoration(
                  color: !Utils.isEnableForecastMode(_cubit.state.forecastType)
                      ? Colors.white
                      : AppColors.greenForecastHex,
                  borderRadius: BorderRadius.all(Radius.circular(25)),
                  boxShadow: AppShadow.boxDarkShadow,
                ),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                margin:
                    EdgeInsets.all(AppDimens.statusMarginHorizontal),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      (!Utils.isEnableForecastMode(_cubit.state.forecastType))
                          ? Icons.visibility
                          : Icons.close,
                      color: AppColors.backgroundBlueDark,
                    ),
                    SizedBox(width: 5),
                    Text(
                      !Utils.isEnableForecastMode(_cubit.state.forecastType)
                          ? "Xem dự báo 3 tháng tới"
                          : "Tắt dự báo 3 tháng tới",
                      style: AppTextStyle.blueDarkS12Bold,
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }

  void _onTouchDotListener({List<LineBarSpot>? lineBarSpots}) {
    try {
      if (_isEnableForecastMode() &&
          _cubit.state.forecastType == ForecastDurationType.NINE_MONTH &&
          (lineBarSpots?[0].x ?? -1) > ForecastDurationType.SIX_MONTH.statValue) {
        /// quanth ko loaị bỏ các màu trùng nhau nếu đang bật chế độ dự báo
        _cubit.updateForecastLBSpots(forecastLBSpots: lineBarSpots);
      } else if (_isEnableForecastMode() &&
          _cubit.state.forecastType == ForecastDurationType.OVER_A_YEAR &&
          (lineBarSpots?[0].x ?? -1) > ForecastDurationType.A_YEAR.statValue) {
        /// quanth ko loaị bỏ các màu trùng nhau nếu đang bật chế độ dự báo
        _cubit.updateForecastLBSpots(forecastLBSpots: lineBarSpots);
      } else if (Utils.isEnableForecastMode(_cubit.state.forecastType) &&
          _cubit.state.forecastType == ForecastDurationType.OVER_ALL_YEAR &&
          (lineBarSpots?[0].x ?? -1) > _cubit.state.forecastTimeLines!.length - 3) {
        /// quanth ko loaị bỏ các màu trùng nhau nếu đang bật chế độ dự báo
        _cubit.updateForecastLBSpots(forecastLBSpots: lineBarSpots);
      } else {
        /// quanth loaị bỏ các màu trùng nhau
        Map<Color, int> colorMap = HashMap<Color, int>();
        List<LineBarSpot> newlineBarSpots = [];

        for (int i = 0; i < (lineBarSpots?.length ?? 0); i++) {
          Color? barColor = lineBarSpots?[i].bar.colors[0];
          if (barColor != null) colorMap[barColor] = i;
        }

        colorMap.values.forEach((index) {
          if (lineBarSpots != null) newlineBarSpots.add(lineBarSpots[index]);
        });
        _cubit.updateForecastLBSpots(forecastLBSpots: newlineBarSpots);
      }
    } catch (e, s) {
      print(s);
    }
  }

  void _onSettingClickListener() {
    // if(item.priorityId.compareTo("")!=0){
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Wrap(children: <Widget>[
              BlocProvider(
                create: (context) {
                  return IIPForeBottomSheetCubit();
                },
                child: _buildBottomSheet(),
              ),
            ]),
          );
        });
    // }
  }

  Widget _buildBottomSheet() {
    return IIPForecastBottomSheetPage(
      iips: _cubit.state.forecastIips,
      selectItemListener: _selectItemListener,
      selectedPresenter: _cubit.state.forecastFilterParams?.chartPresenters,
    );
  }

  void _selectItemListener({List<ChartPresenter>? chartPresenters}) {
    _cubit.filterForecastChart(chartPresenters: chartPresenters);
  }

  void createChartPresents({List<IipDataItemEntity>? iips}) {
    _chartPresenters = [];
    for (int i = 0; i < (iips?.length ?? 0); i++) {
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: iips?[i].value,
        name: iips?[i].name,
      );
      _chartPresenters.add(chartPresenter);
    }
  }

  void _calendarClickedListener() {
    MonthCalendarDialog(
      context: context,
      selectedType: _cubit.state.forecastType ?? ForecastDurationType.SIX_MONTH,
      itemSelected: _itemSelected,
      canShowAll: true,
    ).show();
  }

  void _itemSelected({required ForecastDurationType type}) {
    _cubit.updateForecastType(forecastType: type);
  }

  void _settingClickedListener() {
    _onSettingClickListener();
  }

  bool _isEnableForecastMode() {
    return _cubit.state.forecastType == ForecastDurationType.NINE_MONTH ||
        _cubit.state.forecastType == ForecastDurationType.OVER_A_YEAR;
  }

  Widget _buildMidForecastSmallHeader({List<LineBarSpot>? lineBarSpots}) {
    if (lineBarSpots!.isEmpty)
      return Container(
        height: 50,
      );
    else if (lineBarSpots.length == 1) {
      return _buildMidSmallHeader(lineBarSpots: lineBarSpots);
    }

    String time = "";
    if (lineBarSpots.isNotEmpty) {
      String arr =
      _cubit.state.forecastTimeLines![(lineBarSpots[0].x - 1).toInt()];
      String year = arr.split("-")[0];
      String month = arr.split("-")[1];
      time = "Tháng $month Năm $year";
    }

    return Center(
      child: Container(
        height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (lineBarSpots.isNotEmpty)
                ? Text(
              time,
              style: AppTextStyle.blueDarkS12,
            )
                : Container(),
            Container(
              margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
              child: _buildForecastInfo(lineBarSpots: lineBarSpots),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildForecastInfo({List<LineBarSpot>? lineBarSpots}) {
    /// quanth: theo sắp thứ tự vẽ đường thì max index =0, min index = 1, value index = 2
    int max = 0;
    int min = 1;
    int between = 2;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        /*Container(
          width: 20,
          height: 10,
          color: lineBarSpots?[0].bar.colors[0],
        ),
        SizedBox(
          width: 5,
        ),*/
        Text(
          "Dự báo",
          style: AppTextStyle.blueDarkS9,
        ),
        SizedBox(
          width: 2,
        ),
        Text(
          "${lineBarSpots?[between].y.toStringAsFixed(2)}",
          style: AppTextStyle.redS12Bold,
        ),
        SizedBox(
          width: 8,
        ),
        Text(
          "Khoảng dự báo từ",
          style: AppTextStyle.blueDarkS9,
        ),
        SizedBox(
          width: 3,
        ),
        Text(
          "${lineBarSpots?[min].y.toStringAsFixed(2)}",
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 3,
        ),
        Text(
          "đến",
          style: AppTextStyle.blueDarkS9,
        ),
        SizedBox(
          width: 3,
        ),
        Text(
          "${lineBarSpots?[max].y.toStringAsFixed(2)}",
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  void updateForecastType({ChartPresenter? chartPresenter}) {
    if (_cubit.state.forecastType == ForecastDurationType.SIX_MONTH) {
      _cubit.updateForecastType(
          forecastType: ForecastDurationType.NINE_MONTH,
          chartPresenter: chartPresenter);
    } else if (_cubit.state.forecastType == ForecastDurationType.A_YEAR) {
      _cubit.updateForecastType(
          forecastType: ForecastDurationType.OVER_A_YEAR,
          chartPresenter: chartPresenter);
    } else if (_cubit.state.forecastType == ForecastDurationType.NINE_MONTH) {
      _cubit.updateForecastType(
          forecastType: ForecastDurationType.SIX_MONTH,
          chartPresenter: chartPresenter);
    } else if (_cubit.state.forecastType == ForecastDurationType.OVER_A_YEAR) {
      _cubit.updateForecastType(
          forecastType: ForecastDurationType.A_YEAR,
          chartPresenter: chartPresenter);
    } else if (_cubit.state.forecastType ==
        ForecastDurationType.OVER_ALL_YEAR) {
      _cubit.updateForecastType(
          forecastType: ForecastDurationType.ALL_YEAR,
          chartPresenter: chartPresenter);
    } else if (_cubit.state.forecastType == ForecastDurationType.ALL_YEAR) {
      _cubit.updateForecastType(
          forecastType: ForecastDurationType.OVER_ALL_YEAR,
          chartPresenter: chartPresenter);
    }
  }

  void _showSelectIndexDialog() {
    if (Utils.isEnableForecastMode(_cubit.state.forecastType)) {
      updateForecastType();
    } else {
      SelectIndexDialog(
        context: context,
        chartPresenters: _cubit.state.forecastFilterParams?.chartPresenters,
        itemSelected: _indexSelected,
      ).show();
    }
  }

  void _indexSelected({required ChartPresenter chartPresenter}) {
    // final snackBar = SnackBar(content: Text('Yay! ${chartPresenter.name}'));
    // Find the ScaffoldMessenger in the widget tree
    // and use it to show a SnackBar.
    // ScaffoldMessenger.of(context).showSnackBar(snackBar);
    updateForecastType(chartPresenter: chartPresenter);
  }
}
