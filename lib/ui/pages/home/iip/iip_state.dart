part of 'iip_cubit.dart';

class IipState extends Equatable {
  final IipEntity? iipEntity;
  final IIPForecastEntity? iipForecastEntity;
  final LoadStatus? fetchCPIStatus;

  /// quanth: forecast
  final LoadStatus? forecastFilterStatus;
  LineChartFilterParam? forecastFilterParams;
  ForecastDurationType? forecastType;
  final LoadStatus? forecastLBStatus;
  List<LineBarSpot>? forecastLBSpots = [];

  /// quanth: statistical
  final LoadStatus? statisticalFilterStatus;
  LineChartFilterParam? statisticalFilterParams;
  ForecastDurationType? statisticalType;
  final LoadStatus? statLBStatus;
  List<BarTouchedSpot?>? statLBSpots;

  /// quanth: compare
  final LoadStatus? compareFilterStatus;
  LineChartFilterParam? compareFilterParams;
  ForecastDurationType? compareType;

  final LoadStatus? compareLBStatus;
  List<LineBarSpot>? compareLBSpots = [];


  IipState(
      {this.iipEntity,
      this.iipForecastEntity,
      this.fetchCPIStatus,
      this.forecastFilterStatus,
      this.forecastFilterParams,
      this.forecastType,
      this.forecastLBStatus,
      this.forecastLBSpots,
      this.statisticalFilterStatus,
      this.statisticalFilterParams,
      this.statisticalType,
      this.statLBStatus,
      this.statLBSpots,
      this.compareFilterStatus,
      this.compareFilterParams,
      this.compareType,
      this.compareLBStatus,
      this.compareLBSpots});

  IipState copyWith({
    IipEntity? iipEntity,
    IIPForecastEntity? iipForecastEntity,
    LoadStatus? fetchCPIStatus,
    LoadStatus? forecastFilterStatus,
    LineChartFilterParam? forecastFilterParams,
    ForecastDurationType? forecastType,
    LoadStatus? forecastLBStatus,
    List<LineBarSpot>? forecastLBSpots,
    LoadStatus? statisticalFilterStatus,
    LineChartFilterParam? statisticalFilterParams,
    ForecastDurationType? statisticalType,
    LoadStatus? statLBStatus,
    List<BarTouchedSpot?>? statLBSpots,
    LoadStatus? compareFilterStatus,
    LineChartFilterParam? compareFilterParams,
    ForecastDurationType? compareType,
    LoadStatus? compareLBStatus,
    List<LineBarSpot>? compareLBSpots,
  }) {
    return new IipState(
      iipEntity: iipEntity ?? this.iipEntity,
      iipForecastEntity: iipForecastEntity ?? this.iipForecastEntity,
      fetchCPIStatus: fetchCPIStatus ?? this.fetchCPIStatus,
      forecastFilterStatus: forecastFilterStatus ?? this.forecastFilterStatus,
      forecastFilterParams: forecastFilterParams ?? this.forecastFilterParams,
      forecastType: forecastType ?? this.forecastType,
      forecastLBStatus: forecastLBStatus ?? this.forecastLBStatus,
      forecastLBSpots: forecastLBSpots ?? this.forecastLBSpots,
      statisticalFilterStatus:
          statisticalFilterStatus ?? this.statisticalFilterStatus,
      statisticalFilterParams:
          statisticalFilterParams ?? this.statisticalFilterParams,
      statisticalType: statisticalType ?? this.statisticalType,
      statLBStatus: statLBStatus ?? this.statLBStatus,
      statLBSpots: statLBSpots ?? this.statLBSpots,
      compareFilterStatus: compareFilterStatus ?? this.compareFilterStatus,
      compareFilterParams: compareFilterParams ?? this.compareFilterParams,
      compareType: compareType ?? this.compareType,
      compareLBStatus: compareLBStatus ?? this.compareLBStatus,
      compareLBSpots: compareLBSpots ?? this.compareLBSpots,
    );
  }

  List<IipDataItemEntity>? get forecastIips {
    /*List<IipDataItemEntity> list = iipEntity?.data?.subs ?? [];
    if(list.isNotEmpty){
      switch(forecastType){
        case ForecastDurationType.SIX_MONTH:
          return iipEntity?.data?.subs?.sublist(6,11);
        case ForecastDurationType.A_YEAR:
          return iipEntity?.data?.subs?.sublist(0,11);
        case ForecastDurationType.NINE_MONTH:
          return iipEntity?.data?.subs?.sublist(6,14);
        case ForecastDurationType.OVER_A_YEAR:
          return iipEntity?.data?.subs?.sublist(0,14);
        default:
          return iipEntity?.data?.subs?.sublist(6,11);
      }
    }*/
    return iipEntity?.data?.subs ?? [];
  }

  List<String>? get forecastTimeLines {
    List<String> list = iipEntity?.data?.timeline ?? [];
    int range = Utils.checkInt(forecastType?.statValue ?? 0.0)!;
    if (list.isNotEmpty) {
      switch (forecastType) {
        case ForecastDurationType.SIX_MONTH:
          return list.sublist((list.length - 3) - range, list.length - 3);
        case ForecastDurationType.A_YEAR:
          return list.sublist((list.length - 3) - range, list.length - 3);
        case ForecastDurationType.NINE_MONTH:
          return list.sublist(list.length - range, list.length);
        case ForecastDurationType.OVER_A_YEAR:
          return list.sublist(list.length - range, list.length);
        case ForecastDurationType.ALL_YEAR:
          return list.sublist(0, list.length - 3);
        case ForecastDurationType.OVER_ALL_YEAR:
          return list.sublist(list.length - range, list.length);
        default:
          return list;
      }
    }
    return [];
  }

  List<IipDataItemEntity>? get statisticalIips {
    /*List<IipDataItemEntity> list = iipEntity?.data?.subs ?? [];
    if(list.isNotEmpty){
      switch(statisticalType){
        case ForecastDurationType.SIX_MONTH:
          return iipEntity?.data?.subs?.sublist(6,11);
        case ForecastDurationType.A_YEAR:
          return iipEntity?.data?.subs?.sublist(0,11);
        case ForecastDurationType.NINE_MONTH:
          return iipEntity?.data?.subs?.sublist(6,14);
        case ForecastDurationType.OVER_A_YEAR:
          return iipEntity?.data?.subs?.sublist(0,14);
        default:
          return iipEntity?.data?.subs?.sublist(6,11);
      }
    }*/
    return iipEntity?.data?.subs ?? [];
  }

  List<String>? get statisticalTimeLines {
    List<String> list = iipEntity?.data?.timeline ?? [];
    if (list.isNotEmpty) {
      switch (statisticalType) {
        case ForecastDurationType.SIX_MONTH:
          return iipEntity?.data?.timeline?.sublist(6, 12);
        case ForecastDurationType.A_YEAR:
          return iipEntity?.data?.timeline?.sublist(0, 12);
        case ForecastDurationType.NINE_MONTH:
          return iipEntity?.data?.timeline?.sublist(6, 15);
        case ForecastDurationType.OVER_A_YEAR:
          return iipEntity?.data?.timeline?.sublist(0, 15);
        default:
          return iipEntity?.data?.timeline?.sublist(6, 12);
      }
    }
    return [];
  }

  List<IipDataItemEntity>? get compareIips {
    /*List<IipDataItemEntity> list = iipEntity?.data?.subs ?? [];
    if(list.isNotEmpty){
      switch(compareType){
        case ForecastDurationType.SIX_MONTH:
          return iipEntity?.data?.subs?.sublist(6,11);
        case ForecastDurationType.A_YEAR:
          return iipEntity?.data?.subs?.sublist(0,11);
        case ForecastDurationType.NINE_MONTH:
          return iipEntity?.data?.subs?.sublist(6,14);
        case ForecastDurationType.OVER_A_YEAR:
          return iipEntity?.data?.subs?.sublist(0,14);
        default:
          return iipEntity?.data?.subs?.sublist(6,11);
      }
    }*/
    return iipEntity?.data?.subs ?? [];
  }

  List<String>? get compareTimeLines {
    List<String> list = iipEntity?.data?.timeline ?? [];
    if (list.isNotEmpty) {
      switch (compareType) {
        case ForecastDurationType.SIX_MONTH:
          return iipEntity?.data?.timeline?.sublist(6, 12);
        case ForecastDurationType.A_YEAR:
          return iipEntity?.data?.timeline?.sublist(0, 12);
        case ForecastDurationType.NINE_MONTH:
          return iipEntity?.data?.timeline?.sublist(6, 15);
        case ForecastDurationType.OVER_A_YEAR:
          return iipEntity?.data?.timeline?.sublist(0, 15);
        default:
          return iipEntity?.data?.timeline?.sublist(6, 12);
      }
    }
    return [];
  }

  List<double>? get forecastBETIips {
    List<double> cpies = iipForecastEntity?.data?.cpi ?? [];
    return cpies.map((val) => (val + 100)).toList().sublist(0, 3);
  }

  List<double>? get forecastUPIips {
    List<double> cpies = iipForecastEntity?.data?.upper ?? [];
    return cpies.map((val) => (val + 100)).toList().sublist(0, 3);
  }

  List<double>? get forecastLOWIips {
    List<double> cpies = iipForecastEntity?.data?.lower ?? [];
    return cpies.map((val) => (val + 100)).toList().sublist(0, 3);
  }

  @override
  List<Object> get props => [
        this.fetchCPIStatus ?? LoadStatus.LOADING,
        this.iipEntity ?? IipEntity(),
        this.iipForecastEntity ?? IIPForecastEntity(),
        this.forecastFilterStatus ?? LoadStatus.LOADING,
        this.forecastFilterParams ?? LineChartFilterParam(),
        this.statisticalFilterStatus ?? LoadStatus.LOADING,
        this.statisticalFilterParams ?? LineChartFilterParam(),
        this.compareFilterStatus ?? LoadStatus.LOADING,
        this.compareFilterParams ?? LineChartFilterParam(),
        this.forecastType ?? ForecastDurationType.SIX_MONTH,
        this.statisticalType ?? ForecastDurationType.SIX_MONTH,
        this.compareType ?? ForecastDurationType.SIX_MONTH,
        this.forecastLBSpots ?? [],
        this.forecastLBStatus ?? LoadStatus.LOADING,
        this.statLBSpots ?? [],
        this.statLBStatus ?? LoadStatus.LOADING,
        this.compareLBSpots ?? [],
        this.compareLBStatus ?? LoadStatus.LOADING,
      ];
}
