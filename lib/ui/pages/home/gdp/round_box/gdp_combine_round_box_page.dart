import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/entities/item_sync_chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/gdp/gdp_combine_chart/gdp_combine_chart_page.dart';
import 'package:flutter_base/ui/pages/dialog/month_calendar_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../gdp_cubit.dart';

class GDPCombineRoundBoxPage extends StatefulWidget {
  GDPDataEntity gdp;
  String topRightLargeText = "";
  String topRightSmallText = "";
  String bottomMidText = "";

  GDPCombineRoundBoxPage({
    required this.topRightLargeText,
    required this.topRightSmallText,
    required this.bottomMidText,
    required this.gdp,
  });

  @override
  _GDPCombineRoundBoxPageState createState() => _GDPCombineRoundBoxPageState();
}

class _GDPCombineRoundBoxPageState extends State<GDPCombineRoundBoxPage> {
  late GdpCubit _cubit;
  List<ItemSyncChartPresenter> _statPresenters = [];
  List<ItemSyncChartPresenter> _ratePresenters = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<GdpCubit>(context);
    createSyncChartPresents(gdps: widget.gdp);
    _cubit.updateCombineType(combineType: ForecastDurationType.SIX_MONTH);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<GdpCubit, GdpState>(
            buildWhen: (prev, current) =>
                prev.combineType != current.combineType,
            builder: (context, state) {
              return _buildLargeHeader(text: widget.topRightLargeText);
            },
          ),
          // _buildSmallHeader(text: widget.topRightSmallText),
          _buildMidSmallHeader(),
          _buildDescriptionHeader(),
          Expanded(child: _buildChart()),
          _buildDescription(),
          _buildFooter(text: widget.bottomMidText),
        ],
      ),
    );
  }

  Widget _buildDescription() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.gdpOrangeDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Tỷ lệ",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.gdpBlueDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Giá trị",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({required String text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text,
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          CircleButton(
            icon: Icon(
              Icons.calendar_today_rounded,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _calendarClickedListener,
            forecastType:
                _cubit.state.combineType ?? ForecastDurationType.SIX_MONTH,
          ),
        ],
      ),
    );
  }

  Widget _buildSmallHeader({required String text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Text(
        text,
        style: AppTextStyle.greyS9,
      ),
    );
  }

  Widget _buildMidSmallHeader() {
    return BlocBuilder<GdpCubit, GdpState>(
        buildWhen: (prev, current) =>
            prev.lineBarSpotsStatus != current.lineBarSpotsStatus,
        builder: (context, state) {
          if (state.lineBarSpotsStatus == LoadStatus.LOADING) {
            return Container();
          } else if (state.lineBarSpotsStatus == LoadStatus.FAILURE) {
            return Container();
          }
          if (state.lineBarSpots?.isEmpty ?? true)
            return Container(
              height: 50,
            );
          List<Widget> list = [];
          for (int i = 0; i < (state.lineBarSpots?.length ?? 0); i++) {
            list.add(_buildMidSmallItem(lineBarSpot: state.lineBarSpots?[i]));
          }

          String time = "Năm ${state.lineBarSpots?[0].year ?? ""}";

          return Center(
            child: Container(
              height: 50,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  (state.lineBarSpots?.isNotEmpty ?? false)
                      ? Text(
                          time,
                          style: AppTextStyle.blueDarkS12,
                        )
                      : Container(),
                  Container(
                    margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: list,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget _buildDescriptionHeader() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: Row(
        children: [
          Text(
            "(tỷ đồng)",
            style: AppTextStyle.greyS12,
            textAlign: TextAlign.start,
          ),
          Spacer(),
          Text(
            "(tỷ lệ %)",
            style: AppTextStyle.greyS12,
            textAlign: TextAlign.end,
          ),
        ],
      ),
    );
  }

  Widget _buildMidSmallItem({required ItemSyncChartPresenter? lineBarSpot}) {
    return Row(
      children: [
        Container(
          width: 20,
          height: 10,
          color: lineBarSpot?.key == 0
              ? AppColors.gdpOrangeDark
              : AppColors.gdpBlueDark,
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          lineBarSpot?.value.toString() ?? "",
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.only(bottom: AppDimens.statusMarginHorizontal),
      child: GDPCombineChartPage(
        statPresenters: _statPresenters,
        ratePresenters: _ratePresenters,
        onTouchDotListener: _onTouchDotListener,
        combineType: _cubit.state.combineType ?? ForecastDurationType.SIX_MONTH,
      ),
    );
  }

  Widget _buildFooter({required String text}) {
    return Center(
      child: Container(
        margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
        child: Text(
          text,
          style: AppTextStyle.blueDarkS12Bold,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  List<ItemSyncChartPresenter> _lineBarSpots = [];

  void _onTouchDotListener(
      {required List<ItemSyncChartPresenter> lineBarSpots}) {
    _cubit.updateLineBarSpots(lineBarSpots: lineBarSpots);
  }

  void createSyncChartPresents({required GDPDataEntity gdps}) {
    _statPresenters.clear();
    _ratePresenters.clear();
    for (int i = 0; i < (widget.gdp.year?.length ?? 0); i++) {
      String year = widget.gdp.year?[i].toString() ?? "";
      double stat = widget.gdp.values?[i] ?? 0;
      double rate = widget.gdp.rates?[i] ?? 0;
      ItemSyncChartPresenter statEntity =
          ItemSyncChartPresenter(year: year, value: stat, key: 0);
      ItemSyncChartPresenter rateEntity =
          ItemSyncChartPresenter(year: year, value: rate, key: 1);
      _statPresenters.add(statEntity);
      _ratePresenters.add(rateEntity);
    }
  }

  void _calendarClickedListener() {
    MonthCalendarDialog(
      context: context,
      selectedType: _cubit.state.combineType ?? ForecastDurationType.SIX_MONTH,
      itemSelected: _itemSelected,
      canShowAll: false,
    ).show();
  }

  void _itemSelected({required ForecastDurationType type}) {
    _cubit.updateCombineType(combineType: type);
  }
}
