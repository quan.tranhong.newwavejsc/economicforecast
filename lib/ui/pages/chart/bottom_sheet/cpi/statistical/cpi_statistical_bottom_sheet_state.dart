part of 'cpi_statistical_bottom_sheet_cubit.dart';

@immutable
class CPIStatBottomSheetState extends Equatable {

  final LoadStatus? filterStatus;
  LineChartFilterParam? filterParams;

  CPIStatBottomSheetState({
    this.filterStatus,
    this.filterParams
  });

  CPIStatBottomSheetState copyWith({
    LoadStatus? filterStatus,
    LineChartFilterParam? filterParams,
  }) {
    return new CPIStatBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [this.filterStatus ?? LoadStatus.LOADING, this.filterParams ?? LineChartFilterParam()];

}
