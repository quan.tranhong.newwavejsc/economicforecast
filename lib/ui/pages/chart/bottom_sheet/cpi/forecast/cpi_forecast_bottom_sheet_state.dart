part of 'cpi_forecast_bottom_sheet_cubit.dart';

@immutable
class CPIForeBottomSheetState extends Equatable {
  final LoadStatus? filterStatus;
  LineChartFilterParam? filterParams;

  CPIForeBottomSheetState(
      {this.filterStatus, this.filterParams});

  CPIForeBottomSheetState copyWith({
    LoadStatus? filterStatus,
    LineChartFilterParam? filterParams,
  }) {
    return new CPIForeBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [
        this.filterStatus ?? LoadStatus.LOADING,
        this.filterParams ?? LineChartFilterParam()
      ];
}
