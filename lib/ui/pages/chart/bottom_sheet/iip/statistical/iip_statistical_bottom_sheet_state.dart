part of 'iip_statistical_bottom_sheet_cubit.dart';

@immutable
class IIPStatBottomSheetState extends Equatable {

  final LoadStatus? filterStatus;
  LineChartFilterParam? filterParams;

  IIPStatBottomSheetState({
    this.filterStatus,
    LineChartFilterParam? filterParams
  }) {
    this.filterParams = filterParams ?? LineChartFilterParam();
  }

  IIPStatBottomSheetState copyWith({
    LoadStatus? filterStatus,
    LineChartFilterParam? filterParams,
  }) {
    return new IIPStatBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [this.filterStatus ?? LoadStatus.LOADING, this.filterParams ?? LineChartFilterParam()];

}
