part of 'unem_statistical_bottom_sheet_cubit.dart';

@immutable
class UnemStatBottomSheetState extends Equatable {
  final LoadStatus? filterStatus;
  GrotripChartFilterParam? filterParams;

  UnemStatBottomSheetState(
      {this.filterStatus, GrotripChartFilterParam? filterParams}) {
    this.filterParams = filterParams ?? GrotripChartFilterParam();
  }

  UnemStatBottomSheetState copyWith({
    LoadStatus? filterStatus,
    GrotripChartFilterParam? filterParams,
  }) {
    return new UnemStatBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [
        this.filterStatus ?? LoadStatus.LOADING,
        this.filterParams ?? GrotripChartFilterParam()
      ];
}
