import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_group_presenter.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/params/grotrip_chart_filter_param.dart';
import 'package:meta/meta.dart';

import '../../../../../../models/params/twice_chart_filter_param.dart';

part 'unem_statistical_bottom_sheet_state.dart';

class UnemStatBottomSheetCubit extends Cubit<UnemStatBottomSheetState> {
  UnemStatBottomSheetCubit() : super(UnemStatBottomSheetState());

  void filterIndexChart({
    ChartGroupPresenter? chartGroupPresenter,
  }) async {
    emit(state.copyWith(filterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          filterStatus: LoadStatus.SUCCESS,
          filterParams: GrotripChartFilterParam(
            chartPresenter: chartGroupPresenter,
          ),
        ),
      );
    } catch (error) {
      emit(state.copyWith(filterStatus: LoadStatus.FAILURE));
    }
  }
}
