import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_group_presenter.dart';
import 'package:flutter_base/models/entities/chart_group_reg_presenter.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/enums/unem_type.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/unem/item/unem_stat_bottom_sheet_item.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'unem_statistical_bottom_sheet_cubit.dart';

class UnemStatisBottomSheetPage extends StatefulWidget {
  UnemDataEntity? unem;
  UnemType type;
  ChartGroupPresenter? chartGroupPresenter;
  Function? selectItemListener;

  UnemStatisBottomSheetPage({
    this.unem,
    required this.type,
    this.selectItemListener,
    this.chartGroupPresenter,
  });

  @override
  _UnemStatisBottomSheetPageState createState() =>
      _UnemStatisBottomSheetPageState();
}

class _UnemStatisBottomSheetPageState extends State<UnemStatisBottomSheetPage> {
  List<ChartGroupPresenter> _chartGroupPresenters = [];
  List<ChartGroupRegPresenter> _chartGroupRegPresenters = [];
  late UnemStatBottomSheetCubit _cubit;

  List<GlobalKey<UnemStatBottomSheetItemState>> itemGlobalKeys = [];

  @override
  void initState() {
    super.initState();
    _cubit = context.read<UnemStatBottomSheetCubit>();
    createChartPresents(unem: widget.unem);
    _cubit.filterIndexChart(
      chartGroupPresenter: widget.chartGroupPresenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints:
          BoxConstraints(maxHeight: ScreenSize.of(context).height * 0.8),
      padding: EdgeInsets.only(bottom: 10),
      width: ScreenSize.of(context).width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(19.0),
          topLeft: Radius.circular(19.0),
        ),
      ),
      // height: ScreenSize.of(context).height /3,
      child: Container(
          margin: EdgeInsets.only(top: 10, left: 10, right: 10),
          child: _buildGridView()),
    );
  }

  Widget _buildGridView() {
    itemGlobalKeys.clear();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                  top: 10,
                  left: 5,
                  bottom: 10,
                  right: 5,
                ),
                child: Text(
                  "Các chỉ tiêu",
                  style: AppTextStyle.blueDarkS15Bold,
                ),
              ),
            ),
            Container(width: 10),
            CircleButton(
              icon: Icon(
                Icons.close,
                color: AppColors.backgroundBlueDark,
              ),
              onTapListenter: _closeClickedListener,
            ),
          ],
        ),
        Expanded(
          child: GridView.builder(
            physics: ScrollPhysics(),
            itemCount: _chartGroupPresenters.length,
            shrinkWrap: true,
            primary: false,
            itemBuilder: (context, index) {
              GlobalKey<UnemStatBottomSheetItemState> key = GlobalKey();
              itemGlobalKeys.add(key);
              return _buildItemGrid(
                chartGroupPresenter: _chartGroupPresenters[index],
                key: key,
              );
            },
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1.4, crossAxisCount: 3),
          ),
        ),
        BlocBuilder<UnemStatBottomSheetCubit, UnemStatBottomSheetState>(
            buildWhen: (prev, current) =>
                prev.filterStatus != current.filterStatus,
            builder: (context, state) {
              if (state.filterStatus == LoadStatus.LOADING) {
                return Container();
              } else if (state.filterStatus == LoadStatus.FAILURE) {
                return Container();
              }
              return GestureDetector(
                onTap: () {
                  widget.selectItemListener!(
                    chartGroupPresenter:
                        _cubit.state.filterParams?.chartPresenter,
                  );
                  Navigator.of(context).pop();
                },
                child: SafeArea(
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.backgroundBlueDark,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      boxShadow: AppShadow.boxShadow,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Center(
                      child: Text(
                        "Áp dụng",
                        style: AppTextStyle.whiteS16Bold,
                      ),
                    ),
                  ),
                ),
              );
            }),
      ],
    );
  }

  void _selectItemListener({
    ChartGroupPresenter? chartGroupPresenter,
    bool? isSelected,
  }) {
    if (isSelected ?? false) {
      int index = _cubit.state.filterParams?.chartPresenter?.key ?? -1;
      if (index != -1) {
        itemGlobalKeys[index].currentState?.updateChoiceMatter();
      }
      _cubit.filterIndexChart(
        chartGroupPresenter: chartGroupPresenter,
      );
    }
  }

  Widget _buildItemGrid({
    ChartGroupPresenter? chartGroupPresenter,
    GlobalKey<UnemStatBottomSheetItemState>? key,
  }) {
    bool isSelected = false;
    if (widget.chartGroupPresenter?.key == chartGroupPresenter?.key) isSelected = true;
    return UnemStatBottomSheetItem(
      chartGroupPresenter: chartGroupPresenter,
      selectItemListener: _selectItemListener,
      isSelected: isSelected,
      key: key,
    );
  }

  void createChartPresents({UnemDataEntity? unem}) {
    _chartGroupRegPresenters = [];
    List<UnemDataItemEntity> dataItemEntities = unem?.unemployment ?? [];
    for (int i = 0; i < dataItemEntities.length; i++) {
      List<ChartGroupPresenter> chatGroupPresenters = [];
      UnemDataItemEntity dataItem = dataItemEntities[i];
      String ageName = dataItemEntities[i].indexName ?? "";
      List<UnemValueEntity> valueEntities = dataItem.value ?? [];
      for (int j = 0; j < valueEntities.length; j++) {
        UnemValueEntity valueItem = valueEntities[j];
        String regionName = valueItem.region ?? "";
        UnemValueItemEntity valueItemEntities =
            valueItem.value ?? UnemValueItemEntity();
        ChartPresenter allPresenter = ChartPresenter(
          key: 0,
          value: valueItemEntities.chung,
          name: "chung",
        );
        ChartPresenter femalePresenter = ChartPresenter(
          key: 1,
          value: valueItemEntities.nam,
          name: "nam",
        );
        ChartPresenter malePresenter = ChartPresenter(
          key: 2,
          value: valueItemEntities.nam,
          name: "nu",
        );
        ChartGroupPresenter groupPresenter = ChartGroupPresenter(
          key: j,
          name: regionName,
          presenters: [allPresenter, malePresenter, femalePresenter],
        );
        chatGroupPresenters.add(groupPresenter);
      }
      ChartGroupRegPresenter chartGroupRegPresenter = ChartGroupRegPresenter(
        key: i,
        name: ageName,
        presenters: chatGroupPresenters,
      );
      _chartGroupRegPresenters.add(chartGroupRegPresenter);
    }

    _chartGroupPresenters = [];
    _chartGroupPresenters = _chartGroupRegPresenters[widget.type.value].presenters ?? [];
  }

  void _closeClickedListener() {
    Navigator.of(context).pop();
  }
}
