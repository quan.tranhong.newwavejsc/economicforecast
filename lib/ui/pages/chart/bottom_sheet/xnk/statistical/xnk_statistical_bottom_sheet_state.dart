part of 'xnk_statistical_bottom_sheet_cubit.dart';

@immutable
class XNKStatBottomSheetState extends Equatable {
  final LoadStatus? filterStatus;
  TwiceChartFilterParam? filterParams;

  XNKStatBottomSheetState(
      {this.filterStatus, TwiceChartFilterParam? filterParams}) {
    this.filterParams = filterParams ?? TwiceChartFilterParam();
  }

  XNKStatBottomSheetState copyWith({
    LoadStatus? filterStatus,
    TwiceChartFilterParam? filterParams,
  }) {
    return new XNKStatBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [
        this.filterStatus ?? LoadStatus.LOADING,
        this.filterParams ?? TwiceChartFilterParam()
      ];
}
