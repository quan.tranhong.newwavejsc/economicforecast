import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';

class GDPCompareBottomSheetItem extends StatefulWidget {
  ChartPresenter? chartPresenter;
  bool? isSelected;
  Function? selectItemListener;

  GDPCompareBottomSheetItem({
    Key? key,
    this.chartPresenter,
    this.selectItemListener,
    this.isSelected,
  }) : super(key: key);

  @override
  GDPCompareBottomSheetItemState createState() =>
      GDPCompareBottomSheetItemState();
}

class GDPCompareBottomSheetItemState extends State<GDPCompareBottomSheetItem> {
  late bool _isSelected;

  @override
  void initState() {
    super.initState();
    _isSelected = widget.isSelected ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (!_isSelected) {
            _isSelected = true;
            widget.selectItemListener!(
              chartPresenter: widget.chartPresenter,
              isSelected: _isSelected,
            );
          }
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: !_isSelected ? Colors.white : AppColors.greenForecastHex,
            borderRadius: BorderRadius.circular(12),
            boxShadow: AppShadow.boxLightShadow),
        margin: EdgeInsets.all(5),
        child: Column(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.all(5),
                child: Center(
                  child: Text(
                    widget.chartPresenter?.name ?? "",
                    style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                        color: AppColors.backgroundBlueDark),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void updateChoiceMatter() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }
}
