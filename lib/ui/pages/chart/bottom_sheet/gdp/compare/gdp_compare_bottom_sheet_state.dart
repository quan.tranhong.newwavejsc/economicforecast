part of 'gdp_compare_bottom_sheet_cubit.dart';

@immutable
class GdpCompareBottomSheetState extends Equatable {
  final LoadStatus? filterStatus;
  LineChartFilterParam? filterParams;

  GdpCompareBottomSheetState({this.filterStatus, this.filterParams});

  GdpCompareBottomSheetState copyWith({
    LoadStatus? filterStatus,
    LineChartFilterParam? filterParams,
  }) {
    return new GdpCompareBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [
        this.filterParams ?? LoadStatus.LOADING,
        this.filterStatus ?? LineChartFilterParam()
      ];
}
