import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/gdp/item/gdp_compare_bottom_sheet_item.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'gdp_compare_bottom_sheet_cubit.dart';

class GDPCompareBottomSheetPage extends StatefulWidget {
  GDPDataEntity? gdps;
  ChartPresenter? selectedPresenter;
  Function? selectItemListener;

  GDPCompareBottomSheetPage({
    this.gdps,
    this.selectedPresenter,
    this.selectItemListener,
  });

  @override
  _GDPCompareBottomSheetPageState createState() =>
      _GDPCompareBottomSheetPageState();
}

class _GDPCompareBottomSheetPageState extends State<GDPCompareBottomSheetPage> {
  List<ChartPresenter> _chartPresenters = [];
  late GdpCompareBottomSheetCubit _cubit;

  List<GlobalKey<GDPCompareBottomSheetItemState>> itemGlobalKeys = [];

  @override
  void initState() {
    super.initState();
    _cubit = context.read<GdpCompareBottomSheetCubit>();
    createChartPresents(gdps: widget.gdps);
    _cubit.filterIndexChart(chartPresenter: _chartPresenters[0]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints:
          BoxConstraints(maxHeight: ScreenSize.of(context).height * 0.3),
      padding: EdgeInsets.only(bottom: 10),
      width: ScreenSize.of(context).width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(19.0),
          topLeft: Radius.circular(19.0),
        ),
      ),
      // height: ScreenSize.of(context).height /3,
      child: Container(
          margin: EdgeInsets.only(top: 10, left: 10, right: 10),
          child: _buildGridView()),
    );
  }

  Widget _buildGridView() {
    itemGlobalKeys.clear();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 5,
            bottom: 10,
            right: 5,
          ),
          child: Text(
            "Các chỉ tiêu",
            style: AppTextStyle.blueDarkS15Bold,
          ),
        ),
        Expanded(
          child: GridView.builder(
            physics: ScrollPhysics(),
            itemCount: _chartPresenters.length,
            shrinkWrap: true,
            primary: false,
            itemBuilder: (context, index) {
              GlobalKey<GDPCompareBottomSheetItemState> key = GlobalKey();
              itemGlobalKeys.add(key);
              return _buildItemGrid(
                  chartPresenter: _chartPresenters[index], key: key);
            },
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1.4, crossAxisCount: 3),
          ),
        ),
        BlocBuilder<GdpCompareBottomSheetCubit, GdpCompareBottomSheetState>(
            buildWhen: (prev, current) =>
            prev.filterStatus != current.filterStatus,
            builder: (context, state) {
              if (state.filterStatus == LoadStatus.LOADING) {
                return Container();
              } else if (state.filterStatus == LoadStatus.FAILURE) {
                return Container();
              }
              return GestureDetector(
                onTap: () {
                  widget.selectItemListener!(
                      chartPresenter: _cubit.state.filterParams?.chartPresenter);
                  Navigator.of(context).pop();
                },
                child: SafeArea(
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.backgroundBlueDark,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      boxShadow: AppShadow.boxShadow,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Center(
                      child: Text(
                        "Áp dụng",
                        style: AppTextStyle.whiteS16Bold,
                      ),
                    ),
                  ),
                ),
              );
            }),
      ],
    );
  }

  Widget _buildItemGrid(
      {ChartPresenter? chartPresenter,
      GlobalKey<GDPCompareBottomSheetItemState>? key}) {
    bool isSelected = false;
    if (widget.selectedPresenter == chartPresenter)
      isSelected = true;
    return GDPCompareBottomSheetItem(
      chartPresenter: chartPresenter ?? ChartPresenter(),
      selectItemListener: _selectItemListener,
      isSelected: isSelected,
      key: key!,
    );
  }

  void _selectItemListener(
      {ChartPresenter? chartPresenter, bool? isSelected}) {
    if (isSelected ?? false) {
      int index = _cubit.state.filterParams?.chartPresenter?.key ?? -1;
      if (index != -1) {
        itemGlobalKeys[index].currentState?.updateChoiceMatter();
      }
      _cubit.filterIndexChart(chartPresenter: chartPresenter);
    }
  }

  void createChartPresents({GDPDataEntity? gdps}) {
    _chartPresenters = [];
    for(int i=0; i< 2; i++){
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: i == 0 ? gdps?.values : gdps?.rates,
        name: i == 0 ? "Giá trị" : "Tỷ lệ",
      );
      _chartPresenters.add(chartPresenter);
    }
  }
}
