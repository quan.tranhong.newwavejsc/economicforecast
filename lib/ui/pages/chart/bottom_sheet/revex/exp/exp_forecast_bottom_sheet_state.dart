part of 'exp_forecast_bottom_sheet_cubit.dart';

@immutable
class ExpForecastBottomSheetState extends Equatable {
  final LoadStatus? filterStatus;
  LineChartFilterParam? filterParams;

  ExpForecastBottomSheetState(
      {this.filterStatus, this.filterParams});

  ExpForecastBottomSheetState copyWith({
    LoadStatus? filterStatus,
    LineChartFilterParam? filterParams,
  }) {
    return new ExpForecastBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [
    this.filterStatus ?? LoadStatus.LOADING,
    this.filterParams ?? LineChartFilterParam()
  ];
}
