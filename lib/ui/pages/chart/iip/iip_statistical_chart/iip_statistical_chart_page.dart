import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/pages/home/iip/iip_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class IIPStatChartPage extends StatefulWidget {
  List<String>? timeline;
  Function onTouchDotListener;
  ForecastDurationType? statisticalType;

  IIPStatChartPage(
      {this.timeline,
      required this.onTouchDotListener,
      required this.statisticalType});

  @override
  _IIPStatChartPageState createState() => _IIPStatChartPageState();
}

class _IIPStatChartPageState extends State<IIPStatChartPage> {
  late IipCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<IipCubit>(context);
    _cubit.initStatisticalChartPresenter(
        chartPresenter: _cubit.state.statisticalFilterParams!.chartPresenter);
  }

  double getMinY() {
    double min = 0;
    ChartPresenter? chartPresenter =
        _cubit.state.statisticalFilterParams?.chartPresenter;
    ForecastDurationType type = _cubit.state.statisticalType ?? ForecastDurationType.SIX_MONTH;
    List<double>? list = _createRealValue(chartPresenter?.value ?? []);
    if (list?.isNotEmpty ?? false) {
      for (int i = 0; i < type.statValue; i++) {
        if ((list?[i] ?? 0) < min) min = list?[i] ?? 0.0;
      }
    }
    return min;
  }

  double getMaxY() {
    double? max = 0.0;
    ChartPresenter chartPresenter =
        _cubit.state.statisticalFilterParams!.chartPresenter!;
    ForecastDurationType type = _cubit.state.statisticalType ?? ForecastDurationType.SIX_MONTH;
    List<double>? list = _createRealValue(chartPresenter.value ?? []);
    if (list?.isNotEmpty ?? false) {
      for (int i = 0; i < type.statValue; i++) {
        if ((list?[i] ?? 0.0) > max!) max = list?[i] ?? 0.0;
      }
    }
    return max!;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0, left: 6.0),
              child: BlocBuilder<IipCubit, IipState>(
                  buildWhen: (prev, current) =>
                      prev.statisticalFilterStatus !=
                      current.statisticalFilterStatus,
                  builder: (context, state) {
                    if (state.statisticalFilterStatus == LoadStatus.LOADING) {
                      return Container();
                    } else if (state.statisticalFilterStatus ==
                        LoadStatus.FAILURE) {
                      return Container();
                    }
                    return BarChart(
                      getBarChartData(
                          maxYvalue: widget.statisticalType?.statValue ??
                              ForecastDurationType.SIX_MONTH.statValue),
                      swapAnimationDuration: const Duration(milliseconds: 250),
                    );
                  }),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  BarChartData getBarChartData({double? maxYvalue}) {
    int keyIndex = 5;
    double? minY = double.parse(getMinY().toStringAsFixed(0));
    double? maxY = double.parse(getMaxY().toStringAsFixed(0));
    double? max = (minY.abs() > maxY.abs()) ? minY.abs() : maxY.abs();
    double? i = double.parse((max / keyIndex).toStringAsFixed(0));
    if (max % keyIndex != 0) {
      max = i * keyIndex;
    }
    List<BarChartGroupData> barGroups = getBarGroups(maxYvalue: maxYvalue);
    return BarChartData(
      alignment: BarChartAlignment.spaceAround,
      barTouchData: BarTouchData(
        enabled: true,
        touchCallback: (BarTouchResponse touchResponse) {
          if (touchResponse != null) {
            BarTouchedSpot? spot = touchResponse.spot;
            widget.onTouchDotListener(barTouchedSpot: spot);
          }
        },
        touchTooltipData: BarTouchTooltipData(
          tooltipBgColor: Colors.transparent,
          tooltipMargin: -10,
          getTooltipItem: (
            BarChartGroupData group,
            int groupIndex,
            BarChartRodData rod,
            int rodIndex,
          ) {
            Color color = Colors.transparent;
            return BarTooltipItem(
              "${rod.y}",
              TextStyle(
                  fontSize: 12, color: color, fontWeight: FontWeight.bold),
            );
          },
        ),
      ),
      gridData: FlGridData(
        show: true,
        checkToShowHorizontalLine: (value) => value % keyIndex == 0,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: (value == 0) ? AppColors.backgroundBlueDark : AppColors.gray,
            strokeWidth: (value == 0) ? 1 : 0.5,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          rotateAngle: 60,
          margin: 20,
          getTitles: (value) {
            if (value >= 0 && value < maxYvalue!)
              return widget.timeline![(value).toInt()];
            return '';
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          getTitles: (value) {
            if (value == 0) {
              return '0';
            }
            return '${value.toInt()}';
          },
          checkToShowTitle:
              (minValue, maxValue, sideTitles, appliedInterval, value) =>
                  value % keyIndex == 0,
          margin: 8,
          reservedSize: 25,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: Border(
          bottom: BorderSide(color: Colors.grey, width: 0.5),
          left: BorderSide(
            color: AppColors.backgroundBlueDark,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minY: -1 * max,
      maxY: max,
      barGroups: barGroups,
    );
  }

  BarChartGroupData createBarChartData(
      {int? x, double? y, List<Color>? colors}) {
    return BarChartGroupData(
      x: x ?? 0,
      barRods: [
        BarChartRodData(
          width: ScreenSize.of(context).width / 20,
          y: y ?? 0,
          colors: colors,
          borderRadius: BorderRadius.all(Radius.zero),
        )
      ],
    );
  }

  List<BarChartGroupData> getBarGroups({double? maxYvalue}) {
    ChartPresenter chartPresenter =
        _cubit.state.statisticalFilterParams?.chartPresenter ??
            ChartPresenter();
    if (chartPresenter == null) {
      return [];
    }
    List<BarChartGroupData> result = [];
    List<double>? realValues = _createRealValue(chartPresenter.value ?? []);
    for (int i = 0; i < maxYvalue!; i++) {
      double y = realValues![i];
      result.add(
        createBarChartData(
          x: i,
          y: y,
          colors: [
            (y < 0)
                ? darken(AppColors.chartColors[chartPresenter.key ?? 0], 50)
                : AppColors.chartColors[chartPresenter.key ?? 0],
          ],
        ),
      );
    }
    return result;
  }

  /// Darken a color by [percent] amount (100 = black)
  // ........................................................
  Color darken(Color c, [int percent = 10]) {
    assert(1 <= percent && percent <= 100);
    var f = 1 - percent / 100;
    return Color.fromARGB(c.alpha, (c.red * f).round(), (c.green * f).round(),
        (c.blue * f).round());
  }

  /// Lighten a color by [percent] amount (100 = white)
  // ........................................................
  Color lighten(Color c, [int percent = 10]) {
    assert(1 <= percent && percent <= 100);
    var p = percent / 100;
    return Color.fromARGB(
        c.alpha,
        c.red + ((255 - c.red) * p).round(),
        c.green + ((255 - c.green) * p).round(),
        c.blue + ((255 - c.blue) * p).round());
  }

  List<double>? _createRealValue(List<double>? oldValue) {
    List<double>? realValues = [];
    realValues.addAll(oldValue ?? []);
    if (realValues.isNotEmpty) {
      switch (_cubit.state.statisticalType) {
        case ForecastDurationType.SIX_MONTH:
          realValues = realValues.sublist(6, 12);
          break;
        case ForecastDurationType.A_YEAR:
          realValues = realValues.sublist(0, 12);
          break;
        case ForecastDurationType.NINE_MONTH:
          realValues = realValues.sublist(6, 15);
          break;
        case ForecastDurationType.OVER_A_YEAR:
          realValues = realValues.sublist(0, 15);
          break;
        default:
          realValues = realValues.sublist(6, 12);
      }
    }
    return realValues;
  }
}
