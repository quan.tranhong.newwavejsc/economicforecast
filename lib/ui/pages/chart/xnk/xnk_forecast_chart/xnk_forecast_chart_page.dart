import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/pages/home/xnk/xnk_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class XNKForecastChartPage extends StatefulWidget {
  ForecastDurationType? forecastType;
  Function onTouchDotListener;

  XNKForecastChartPage(
      {this.forecastType, required this.onTouchDotListener});

  @override
  _XNKForecastChartPageState createState() => _XNKForecastChartPageState();
}

class _XNKForecastChartPageState extends State<XNKForecastChartPage> {
  late XnkCubit _cubit;
  int touchedIndex = 0;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<XnkCubit>(context);
    _cubit.initForecastChartPresenter(
      imChartPresenter: _cubit.state.forecastFilterParams?.imChartPresenter,
      exChartPresenter: _cubit.state.forecastFilterParams?.exChartPresenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0, left: 6.0),
              child: BlocBuilder<XnkCubit, XnkState>(
                  buildWhen: (prev, current) =>
                      prev.forecastFilterStatus !=
                          current.forecastFilterStatus ||
                      prev.forecastType != current.forecastType,
                  builder: (context, state) {
                    if (state.forecastFilterStatus == LoadStatus.LOADING) {
                      return Container();
                    } else if (state.forecastFilterStatus ==
                        LoadStatus.FAILURE) {
                      return Container();
                    }
                    return LineChart(
                      createChartData(
                          maxYvalue: _cubit.state.forecastType?.value ??
                              ForecastDurationType.SIX_MONTH.value),
                      swapAnimationDuration: const Duration(milliseconds: 250),
                    );
                  }),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  LineChartData createChartData({double? maxYvalue}) {
    List<LineChartBarData> lineBarsDatas = linesBarData(maxYValue: maxYvalue!);
    return LineChartData(
      lineTouchData: LineTouchData(
        enabled: true,
        touchCallback: (LineTouchResponse? touchResponse) {
          if (touchResponse != null) {
            List<LineBarSpot>? lineBarSpots = touchResponse.lineBarSpots;
            widget.onTouchDotListener(lineBarSpots: lineBarSpots);
          }
        },
        getTouchedSpotIndicator:
            (LineChartBarData barData, List<int> spotIndexes) {
          return spotIndexes.map((index) {
            return TouchedSpotIndicatorData(
              FlLine(
                color: Colors.pink,
              ),
              FlDotData(
                show: true,
                getDotPainter: (spot, percent, barData, index) =>
                    FlDotCirclePainter(
                  radius: 8,
                  color: Colors.red,
                  strokeWidth: 2,
                  strokeColor: Colors.black,
                ),
              ),
            );
          }).toList();
        },
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.pink,
          tooltipRoundedRadius: 8,
          getTooltipItems: (List<LineBarSpot> lineBarsSpot) {
            return lineBarsSpot.map((lineBarSpot) {
              return null;
            }).toList();
          },
        ),
      ),
      gridData: FlGridData(
        show: true,
      ),
      titlesData: FlTitlesData(
        /// quanth: danh sách giá trị trục hoành
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          rotateAngle: 60,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          margin: 10,
          getTitles: (value) {
            if (value > 0 && value < maxYvalue)
              return _cubit.state.forecastTimeLines![(value - 1).toInt()];
            return '';
          },
        ),

        /// quanth: danh sách giá trị trục tung
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          getTitles: (value) {
            return '${value.toInt()}';
          },
          margin: 8,
          reservedSize: 25,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(
            color: AppColors.gray,
          ),
          left: BorderSide(
            color: AppColors.gray,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minX: 0,
      maxX: maxYvalue,
      lineBarsData: lineBarsDatas,
    );
  }

  List<LineChartBarData> linesBarData({double? maxYValue}) {
    if (_cubit.state.forecastFilterParams!.imChartPresenter == null ||
        _cubit.state.forecastFilterParams!.exChartPresenter == null) {
      return [];
    }
    List<LineChartBarData> result = [];
    List<double>? realImValues = _createRealValue(
        _cubit.state.forecastFilterParams!.imChartPresenter!.value ?? []);
    List<double>? realExValues = _createRealValue(
        _cubit.state.forecastFilterParams!.exChartPresenter!.value ?? []);

    result.add(
      createLineChartData(
          list: realImValues,
          colors: [
            AppColors.xnkBlueDark,
          ]),
    );
    result.add(
      createLineChartData(
          list: realExValues,
          colors: [
            AppColors.xnkRedDark,
          ]),
    );

    return result;
  }

  LineChartBarData createLineChartData(
      {List<double>? list, List<Color>? colors}) {
    List<FlSpot> flSpots = [];
    ForecastDurationType type =
        _cubit.state.forecastType ?? ForecastDurationType.SIX_MONTH;
    for (int i = 0; i < (type.value - 1); i++) {
      flSpots.add(FlSpot(i + 1, list![i]));
    }
    return LineChartBarData(
      spots: flSpots,
      isCurved: true,
      curveSmoothness: 0,
      colors: colors,
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(show: true),
      belowBarData: BarAreaData(show: false),
    );
  }

  LineChartBarData createDashLineChartData(
      {List<double>? list, List<Color>? colors}) {
    List<FlSpot> flSpots = [];
    ForecastDurationType type =
        _cubit.state.forecastType ?? ForecastDurationType.SIX_MONTH;
    for (int i = 0; i < (type.value - 1); i++) {
      flSpots.add(FlSpot(i + 1, list![i]));
    }
    return LineChartBarData(
      spots: flSpots,
      isCurved: true,
      curveSmoothness: 0,
      colors: colors,
      barWidth: 2,
      dashArray: [2, 4],
      isStrokeCapRound: true,
      dotData: FlDotData(show: true),
      belowBarData: BarAreaData(show: false),
    );
  }

  /// Lerps between a [LinearGradient] colors, based on [t]
  Color? lerpGradient(List<Color> colors, List<double> stops, double t) {
    if (stops.length != colors.length) {
      stops = [];

      /// provided gradientColorStops is invalid and we calculate it here
      colors.asMap().forEach((index, color) {
        final percent = 1.0 / colors.length;
        stops.add(percent * index);
      });
    }

    for (var s = 0; s < stops.length - 1; s++) {
      final leftStop = stops[s], rightStop = stops[s + 1];
      final leftColor = colors[s], rightColor = colors[s + 1];
      if (t <= leftStop) {
        return leftColor;
      } else if (t < rightStop) {
        final sectionT = (t - leftStop) / (rightStop - leftStop);
        return Color.lerp(leftColor, rightColor, sectionT);
      }
    }
    return colors.last;
  }

  List<double>? _createRealValue(List<double>? oldValue) {
    List<double>? realValues = [];
    realValues.addAll(oldValue ?? []);
    if (realValues.isNotEmpty) {
      switch (_cubit.state.forecastType) {
        case ForecastDurationType.SIX_MONTH:
          realValues = realValues.sublist(6, 12);
          break;
        case ForecastDurationType.A_YEAR:
          realValues = realValues.sublist(0, 12);
          break;
        case ForecastDurationType.NINE_MONTH:
          realValues = realValues.sublist(6, 15);
          break;
        case ForecastDurationType.OVER_A_YEAR:
          realValues = realValues.sublist(0, 15);
          break;
        default:
          realValues = realValues.sublist(6, 12);
      }
    }
    return realValues;
  }
}
