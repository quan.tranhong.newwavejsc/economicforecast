import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/pages/home/cpi/cpi_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CPIStatChartPage extends StatefulWidget {
  Function onTouchDotListener;
  ForecastDurationType? statisticalType;

  CPIStatChartPage(
      {required this.onTouchDotListener, required this.statisticalType});

  @override
  _CPIStatChartPageState createState() => _CPIStatChartPageState();
}

class _CPIStatChartPageState extends State<CPIStatChartPage> {
  late CpiCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<CpiCubit>(context);
    _cubit.initStatisticalChartPresenter(
        chartPresenter: _cubit.state.statisticalFilterParams!.chartPresenter);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0, left: 6.0),
              child: BlocBuilder<CpiCubit, CpiState>(
                  buildWhen: (prev, current) =>
                      prev.statisticalFilterStatus !=
                      current.statisticalFilterStatus,
                  builder: (context, state) {
                    if (state.statisticalFilterStatus == LoadStatus.LOADING) {
                      return Container();
                    } else if (state.statisticalFilterStatus ==
                        LoadStatus.FAILURE) {
                      return Container();
                    }
                    return BarChart(
                      getBarChartData(
                          maxYvalue: widget.statisticalType?.statValue ??
                              ForecastDurationType.SIX_MONTH.statValue),
                      swapAnimationDuration: const Duration(milliseconds: 250),
                    );
                  }),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  BarChartData getBarChartData({double? maxYvalue}) {
    List<BarChartGroupData> barGroups = getBarGroups(maxYvalue: maxYvalue);
    return BarChartData(
      alignment: BarChartAlignment.spaceAround,
      barTouchData: BarTouchData(
        enabled: true,
        touchCallback: (BarTouchResponse touchResponse) {
          if (touchResponse != null) {
            BarTouchedSpot? spot = touchResponse.spot;
            widget.onTouchDotListener(barTouchedSpot: spot);
          }
        },
        touchTooltipData: BarTouchTooltipData(
          tooltipBgColor: Colors.transparent,
          tooltipMargin: -10,
          getTooltipItem: (
            BarChartGroupData group,
            int groupIndex,
            BarChartRodData rod,
            int rodIndex,
          ) {
            Color color = Colors.transparent;
            return BarTooltipItem(
              "",
              TextStyle(
                  fontSize: 12, color: color, fontWeight: FontWeight.bold),
            );
          },
        ),
      ),
      gridData: FlGridData(
        show: true,
        checkToShowHorizontalLine: (value) => value % 10 == 0,
        getDrawingHorizontalLine: (value) => FlLine(
          color: const Color(0xffe7e8ec),
          strokeWidth: 1,
        ),
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          rotateAngle: 60,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          margin: 10,
          getTitles: (value) {
            if (value >= 0 && value < maxYvalue!)
              return _cubit.state.statisticalTimeLines![(value).toInt()];
            return '';
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context,value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          getTitles: (value) {
            if (value >= 90 && value % 5 == 0) return '$value';
            return '';
          },
          margin: 8,
          reservedSize: 25,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: Border(
          bottom: BorderSide(
            color: AppColors.gray,
          ),
          left: BorderSide(
            color: AppColors.gray,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minY: 95,
      barGroups: barGroups,
    );
  }

  BarChartGroupData createBarChartData(
      {int? x, double? y, List<Color>? colors}) {
    return BarChartGroupData(
      x: x!,
      barRods: [
        BarChartRodData(
          width: ScreenSize.of(context).width / 20,
          y: y!,
          colors: colors,
          borderRadius: BorderRadius.all(Radius.zero),
        )
      ],
      showingTooltipIndicators: [0],
    );
  }

  List<BarChartGroupData> getBarGroups({double? maxYvalue}) {
    ChartPresenter? chartPresenter =
        _cubit.state.statisticalFilterParams!.chartPresenter;
    if (chartPresenter == null) {
      return [];
    } else {
      List<BarChartGroupData> result = [];
      List<double>? realValues = _createRealValue(chartPresenter.value ?? []);
      for (int i = 0; i < maxYvalue!; i++) {
        double y = realValues![i];
        result.add(
          createBarChartData(
            x: i,
            y: y,
            colors: [AppColors.chartColors[chartPresenter.key ?? 0]],
          ),
        );
      }
      return result;
    }
  }

  List<double>? _createRealValue(List<double>? oldValue) {
    List<double>? realValues = [];
    realValues.addAll(oldValue ?? []);
    if (realValues.isNotEmpty) {
      switch (_cubit.state.statisticalType) {
        case ForecastDurationType.SIX_MONTH:
          realValues = realValues.sublist(6, 12);
          break;
        case ForecastDurationType.A_YEAR:
          realValues = realValues.sublist(0, 12);
          break;
        case ForecastDurationType.NINE_MONTH:
          realValues = realValues.sublist(6, 15);
          break;
        case ForecastDurationType.OVER_A_YEAR:
          realValues = realValues.sublist(0, 15);
          break;
        default:
          realValues = realValues.sublist(6, 12);
      }
    }
    return realValues;
  }
}
