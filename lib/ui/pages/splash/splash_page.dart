import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/commons/app_images.dart';
import 'package:flutter_base/configs/app_config.dart';
import 'package:flutter_base/repositories/auth_repository.dart';
import 'package:flutter_base/router/application.dart';
import 'package:flutter_base/router/routers.dart';
import 'package:flutter_base/ui/pages/splash/splash_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SplashPageState();
  }
}

class _SplashPageState extends State<SplashPage> {
  late SplashCubit _cubit;
  late StreamSubscription _navigationSubscription;

  void initPlatformMethod() {
    FlutterBridge.platform.setMethodCallHandler(_handleMethod);
  }

  Future<dynamic> _handleMethod(MethodCall call) async {
    switch (call.method) {
      case "message":
        debugPrint(call.arguments);
        return new Future.value("");
      case "_retrieveSSOToken":
        String token = call.arguments;
        if (token.trim().isNotEmpty) {
          _cubit.saveToken(token= token);
        } else {
          // quanth: delay 5s mới cho hiện các màn hình khác
          showSignIn();
          //Timer(Duration(seconds: 5), () => _cubit.checkLogin());
        }
    }
  }

  @override
  void initState() {
    final authRepository = RepositoryProvider.of<AuthRepository>(context);
    initPlatformMethod();
    _cubit = SplashCubit(repository: authRepository);
    super.initState();
    _navigationSubscription = _cubit.navigatorController.stream.listen((event) {
      switch (event) {
        case SplashNavigator.OPEN_HOME:
          showHome();
          break;
        case SplashNavigator.OPEN_SIGN_IN:
          showSignIn();
          break;
      }
    });
    if(Platform.isAndroid)
      FlutterBridge.checkToken();
    else if(Platform.isIOS)
      Timer(Duration(seconds: 5), () => _cubit.checkLogin());
  }

  @override
  void dispose() {
    _navigationSubscription.cancel();
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 150,
          decoration: BoxDecoration(color: Colors.transparent),
          alignment: Alignment.center, // This is needed
          child: Image.asset(
            AppImages.icSplashLogo,
            fit: BoxFit.contain,
            width: 150,
          ),
        ),
      ),
    );
  }

  ///Navigate
  void showSignIn() async {
    await Application.router?.navigateTo(context, Routes.signIn);
    _cubit.checkLogin();
  }

  void showHome() {
    Application.router?.navigateTo(context, Routes.home);
  }
}
