// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a vi locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'vi';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "bottom_title":
            MessageLookupByLibrary.simpleMessage("của tỉnh Đồng Nai năm 2017"),
        "bottom_title_compare": MessageLookupByLibrary.simpleMessage(
            "của tỉnh Đồng Nai so với cả nước năm 2017"),
        "cpi_title":
            MessageLookupByLibrary.simpleMessage("Chỉ số giá tiêu dùng (CPI)"),
        "economic_forecast":
            MessageLookupByLibrary.simpleMessage("Dự báo kinh tế"),
        "export_title": MessageLookupByLibrary.simpleMessage("Xuất khẩu"),
        "gdp_title":
            MessageLookupByLibrary.simpleMessage("Tổng sản phẩm nội địa (GDP)"),
        "iip_title": MessageLookupByLibrary.simpleMessage(
            "Chỉ số sản xuất công nghiệp (IIP)"),
        "imex_title": MessageLookupByLibrary.simpleMessage("Thu chi ngân sách"),
        "import_title": MessageLookupByLibrary.simpleMessage("Nhập khẩu"),
        "loading": MessageLookupByLibrary.simpleMessage("Đang tải dữ liệu..."),
        "login": MessageLookupByLibrary.simpleMessage("Đăng nhập"),
        "movies": MessageLookupByLibrary.simpleMessage("Phim ảnh"),
        "password": MessageLookupByLibrary.simpleMessage("Mật khẩu"),
        "register": MessageLookupByLibrary.simpleMessage("Đăng ký tài khoản"),
        "unemployment_title":
            MessageLookupByLibrary.simpleMessage("Tỷ lệ thất nghiệp"),
        "username": MessageLookupByLibrary.simpleMessage("Tên đăng nhập")
      };
}
