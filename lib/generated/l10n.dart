// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Movies`
  String get movies {
    return Intl.message(
      'Movies',
      name: 'movies',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get username {
    return Intl.message(
      'Username',
      name: 'username',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Sign up for an account`
  String get register {
    return Intl.message(
      'Sign up for an account',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `Loading data...`
  String get loading {
    return Intl.message(
      'Loading data...',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `Economic Forecast`
  String get economic_forecast {
    return Intl.message(
      'Economic Forecast',
      name: 'economic_forecast',
      desc: '',
      args: [],
    );
  }

  /// `Consumer Price Index (CPI)`
  String get cpi_title {
    return Intl.message(
      'Consumer Price Index (CPI)',
      name: 'cpi_title',
      desc: '',
      args: [],
    );
  }

  /// `of Dong Nai province in 2017`
  String get bottom_title {
    return Intl.message(
      'of Dong Nai province in 2017',
      name: 'bottom_title',
      desc: '',
      args: [],
    );
  }

  /// `of Dong Nai province compared to the whole country in 2017`
  String get bottom_title_compare {
    return Intl.message(
      'of Dong Nai province compared to the whole country in 2017',
      name: 'bottom_title_compare',
      desc: '',
      args: [],
    );
  }

  /// `Index of Industrial Production (IIP)`
  String get iip_title {
    return Intl.message(
      'Index of Industrial Production (IIP)',
      name: 'iip_title',
      desc: '',
      args: [],
    );
  }

  /// `Gross Domestic Product (GDP)`
  String get gdp_title {
    return Intl.message(
      'Gross Domestic Product (GDP)',
      name: 'gdp_title',
      desc: '',
      args: [],
    );
  }

  /// `Imports`
  String get import_title {
    return Intl.message(
      'Imports',
      name: 'import_title',
      desc: '',
      args: [],
    );
  }

  /// `Exports`
  String get export_title {
    return Intl.message(
      'Exports',
      name: 'export_title',
      desc: '',
      args: [],
    );
  }

  /// `Budget revenue and expenditure`
  String get imex_title {
    return Intl.message(
      'Budget revenue and expenditure',
      name: 'imex_title',
      desc: '',
      args: [],
    );
  }

  /// `Unemployment rate`
  String get unemployment_title {
    return Intl.message(
      'Unemployment rate',
      name: 'unemployment_title',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'vi'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
