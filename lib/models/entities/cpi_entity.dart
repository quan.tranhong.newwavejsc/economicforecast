import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'cpi_entity.g.dart';

@JsonSerializable()
class CPIEntity {
  @JsonKey(name: 'data')
  CPIDataEntity? data;

  CPIEntity({this.data});

  factory CPIEntity.fromJson(Map<String, dynamic> json) => _$CPIEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CPIEntityToJson(this);
}