// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'iip_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IipDataEntity _$IipDataEntityFromJson(Map<String, dynamic> json) {
  return IipDataEntity(
    subs: (json['subs'] as List<dynamic>?)
        ?.map((e) => IipDataItemEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    iip: (json['iip'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    timeline:
        (json['timeline'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$IipDataEntityToJson(IipDataEntity instance) =>
    <String, dynamic>{
      'subs': instance.subs,
      'timeline': instance.timeline,
      'iip': instance.iip,
    };
