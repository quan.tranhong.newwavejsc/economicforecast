import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'imex_entity.g.dart';

@JsonSerializable()
class ImexEntity {
  @JsonKey(name: 'data')
  ImexDataEntity? data;

  ImexEntity({this.data});

  factory ImexEntity.fromJson(Map<String, dynamic> json) => _$ImexEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ImexEntityToJson(this);
}