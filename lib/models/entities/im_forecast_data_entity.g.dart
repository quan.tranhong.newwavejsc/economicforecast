// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'im_forecast_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImForecastDataEntity _$ImForecastDataEntityFromJson(Map<String, dynamic> json) {
  return ImForecastDataEntity(
    import: (json['import'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    timeline:
        (json['timeline'] as List<dynamic>?)?.map((e) => e as String).toList(),
    lower: (json['lower'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    upper: (json['upper'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
  );
}

Map<String, dynamic> _$ImForecastDataEntityToJson(
        ImForecastDataEntity instance) =>
    <String, dynamic>{
      'import': instance.import,
      'timeline': instance.timeline,
      'lower': instance.lower,
      'upper': instance.upper,
    };
