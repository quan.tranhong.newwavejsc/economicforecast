// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'imex_data_item_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImexDataItemEntity _$ImexDataItemEntityFromJson(Map<String, dynamic> json) {
  return ImexDataItemEntity(
    indexName: json['index_name'] as String?,
    value: (json['value'] as List<dynamic>?)
        ?.map((e) => ImexValueEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ImexDataItemEntityToJson(ImexDataItemEntity instance) =>
    <String, dynamic>{
      'index_name': instance.indexName,
      'value': instance.value,
    };
