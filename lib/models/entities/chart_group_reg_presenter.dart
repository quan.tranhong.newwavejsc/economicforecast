

import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';

import 'chart_group_presenter.dart';

class ChartGroupRegPresenter extends Equatable {
  List<ChartGroupPresenter>? presenters;
  int? key;
  String? name;

  ChartGroupRegPresenter({this.presenters, this.key, this.name});

  @override
  List<Object> get props => [this.key ?? 0, this.presenters ?? [], this.name ?? ""];

}