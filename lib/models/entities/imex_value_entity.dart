import 'package:json_annotation/json_annotation.dart';

part 'imex_value_entity.g.dart';

@JsonSerializable()
class ImexValueEntity {
  @JsonKey(name: 'cat_name')
  String? catName;
  @JsonKey(name: 'value')
  List<double>? value;

  ImexValueEntity({this.catName, this.value});

  factory ImexValueEntity.fromJson(Map<String, dynamic> json) => _$ImexValueEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ImexValueEntityToJson(this);

}