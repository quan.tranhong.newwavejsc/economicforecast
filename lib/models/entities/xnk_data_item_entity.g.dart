// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'xnk_data_item_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

XNKDataItemEntity _$XNKDataItemEntityFromJson(Map<String, dynamic> json) {
  return XNKDataItemEntity(
    name: json['name'] as String?,
    rate: (json['rate'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    val: (json['val'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
  );
}

Map<String, dynamic> _$XNKDataItemEntityToJson(XNKDataItemEntity instance) =>
    <String, dynamic>{
      'name': instance.name,
      'rate': instance.rate,
      'val': instance.val,
    };
