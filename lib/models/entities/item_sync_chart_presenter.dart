

import 'package:equatable/equatable.dart';

class ItemSyncChartPresenter extends Equatable {
  double? value;
  String? year;
  String? name;
  int? key;

  ItemSyncChartPresenter({this.value, this.year, this.name, this.key});

  @override
  List<Object> get props => [this.value ?? 0, this.year ?? "", this.name ?? "", this.key?? 0];

}