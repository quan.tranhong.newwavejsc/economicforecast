// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'iip_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IipEntity _$IipEntityFromJson(Map<String, dynamic> json) {
  return IipEntity(
    data: json['data'] == null
        ? null
        : IipDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$IipEntityToJson(IipEntity instance) => <String, dynamic>{
      'data': instance.data,
    };
