import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'cpi_data_entity.g.dart';

@JsonSerializable()
class CPIDataEntity {
  @JsonKey(name: 'cpi')
  List<CpiDataItemEntity>? cpi;
  @JsonKey(name: 'timeline')
  List<String>? timeline;

  CPIDataEntity({this.cpi, this.timeline});

  factory CPIDataEntity.fromJson(Map<String, dynamic> json) => _$CPIDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CPIDataEntityToJson(this);
}