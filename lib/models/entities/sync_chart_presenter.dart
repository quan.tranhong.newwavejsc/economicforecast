

import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/item_sync_chart_presenter.dart';

class SyncChartPresenter extends Equatable {
  List<ItemSyncChartPresenter>? value;
  int? key;
  String? name;

  SyncChartPresenter({this.key, this.value, this.name});

  @override
  List<Object> get props => [this.key ?? 0, this.value ?? "", this.name ?? ""];

}