// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'iip_forecast_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IIPForecastEntity _$IIPForecastEntityFromJson(Map<String, dynamic> json) {
  return IIPForecastEntity(
    data: json['data'] == null
        ? null
        : IIPForecastDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$IIPForecastEntityToJson(IIPForecastEntity instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
