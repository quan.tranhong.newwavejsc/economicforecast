import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'gdp_entity.g.dart';

@JsonSerializable()
class GDPEntity {
  @JsonKey(name: 'data')
  GDPDataEntity? data;

  GDPEntity({this.data});

  factory GDPEntity.fromJson(Map<String, dynamic> json) => _$GDPEntityFromJson(json);

  Map<String, dynamic> toJson() => _$GDPEntityToJson(this);
}