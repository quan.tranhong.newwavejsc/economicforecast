import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'im_forecast_data_entity.g.dart';

@JsonSerializable()
class ImForecastDataEntity {
  @JsonKey(name: 'import')
  List<double>? import;
  @JsonKey(name: 'timeline')
  List<String>? timeline;
  @JsonKey(name: 'lower')
  List<double>? lower;
  @JsonKey(name: 'upper')
  List<double>? upper;

  ImForecastDataEntity({this.import, this.timeline, this.lower, this.upper});

  factory ImForecastDataEntity.fromJson(Map<String, dynamic> json) => _$ImForecastDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ImForecastDataEntityToJson(this);
}