import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'ex_forecast_entity.g.dart';

@JsonSerializable()
class ExForecastEntity {
  @JsonKey(name: 'data')
  ExForecastDataEntity? data;

  ExForecastEntity({this.data});

  factory ExForecastEntity.fromJson(Map<String, dynamic> json) => _$ExForecastEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ExForecastEntityToJson(this);
}