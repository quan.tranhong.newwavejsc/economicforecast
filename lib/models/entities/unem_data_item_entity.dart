import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'unem_data_item_entity.g.dart';

@JsonSerializable()
class UnemDataItemEntity {
  @JsonKey(name: 'index_name')
  String? indexName;
  @JsonKey(name: 'value')
  List<UnemValueEntity>? value;

  UnemDataItemEntity({this.indexName, this.value});

  factory UnemDataItemEntity.fromJson(Map<String, dynamic> json) => _$UnemDataItemEntityFromJson(json);

  Map<String, dynamic> toJson() => _$UnemDataItemEntityToJson(this);

}