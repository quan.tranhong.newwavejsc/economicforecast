import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'iip_entity.g.dart';

@JsonSerializable()
class IipEntity {
  @JsonKey(name: 'data')
  IipDataEntity? data;

  IipEntity({this.data});

  factory IipEntity.fromJson(Map<String, dynamic> json) => _$IipEntityFromJson(json);

  Map<String, dynamic> toJson() => _$IipEntityToJson(this);
}