// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expenditure_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpenditureDataEntity _$ExpenditureDataEntityFromJson(
    Map<String, dynamic> json) {
  return ExpenditureDataEntity(
    details: (json['details'] as List<dynamic>?)
        ?.map((e) =>
            ExpenditureDataDetailEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    domesticExport: json['domestic_export'] == null
        ? null
        : ExpenditureDataDetailEntity.fromJson(
            json['domestic_export'] as Map<String, dynamic>),
    foreignExport: json['foreign_export'] == null
        ? null
        : ExpenditureDataDetailEntity.fromJson(
            json['foreign_export'] as Map<String, dynamic>),
    totalExport: json['total_export'] == null
        ? null
        : ExpenditureDataDetailEntity.fromJson(
            json['total_export'] as Map<String, dynamic>),
    timeline:
        (json['timeline'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$ExpenditureDataEntityToJson(
        ExpenditureDataEntity instance) =>
    <String, dynamic>{
      'details': instance.details,
      'domestic_export': instance.domesticExport,
      'foreign_export': instance.foreignExport,
      'total_export': instance.totalExport,
      'timeline': instance.timeline,
    };
