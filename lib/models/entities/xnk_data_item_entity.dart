import 'package:json_annotation/json_annotation.dart';

part 'xnk_data_item_entity.g.dart';

@JsonSerializable()
class XNKDataItemEntity {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'rate')
  List<double>? rate;
  @JsonKey(name: 'val')
  List<double>? val;

  XNKDataItemEntity({this.name, this.rate, this.val});

  factory XNKDataItemEntity.fromJson(Map<String, dynamic> json) => _$XNKDataItemEntityFromJson(json);

  Map<String, dynamic> toJson() => _$XNKDataItemEntityToJson(this);
}