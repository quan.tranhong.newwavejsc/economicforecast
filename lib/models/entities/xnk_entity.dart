
import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'xnk_entity.g.dart';

@JsonSerializable()
class XNKEntity {
  @JsonKey(name: 'data')
  XNKDataEntity? data;

  XNKEntity({this.data});

  factory XNKEntity.fromJson(Map<String, dynamic> json) => _$XNKEntityFromJson(json);

  Map<String, dynamic> toJson() => _$XNKEntityToJson(this);
}
