// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'iip_data_item_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IipDataItemEntity _$IipDataItemEntityFromJson(Map<String, dynamic> json) {
  return IipDataItemEntity(
    name: json['name'] as String?,
    value: (json['value'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
  );
}

Map<String, dynamic> _$IipDataItemEntityToJson(IipDataItemEntity instance) =>
    <String, dynamic>{
      'name': instance.name,
      'value': instance.value,
    };
