// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'iip_forecast_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IIPForecastDataEntity _$IIPForecastDataEntityFromJson(
    Map<String, dynamic> json) {
  return IIPForecastDataEntity(
    cpi: (json['iip'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    timeline:
        (json['timeline'] as List<dynamic>?)?.map((e) => e as String).toList(),
  )
    ..lower = (json['lower'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList()
    ..upper = (json['upper'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList();
}

Map<String, dynamic> _$IIPForecastDataEntityToJson(
        IIPForecastDataEntity instance) =>
    <String, dynamic>{
      'iip': instance.cpi,
      'timeline': instance.timeline,
      'lower': instance.lower,
      'upper': instance.upper,
    };
