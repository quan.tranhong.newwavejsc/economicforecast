import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'cpi_forecast_entity.g.dart';

@JsonSerializable()
class CPIForecastEntity {
  @JsonKey(name: 'data')
  CPIForecastDataEntity? data;

  CPIForecastEntity({this.data});

  factory CPIForecastEntity.fromJson(Map<String, dynamic> json) => _$CPIForecastEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CPIForecastEntityToJson(this);
}