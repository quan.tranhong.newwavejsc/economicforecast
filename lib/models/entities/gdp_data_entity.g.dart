// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gdp_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GDPDataEntity _$GDPDataEntityFromJson(Map<String, dynamic> json) {
  return GDPDataEntity(
    rates: (json['rates'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    valueUnit: json['value_unit'] as String?,
    values: (json['values'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    year: (json['year'] as List<dynamic>?)?.map((e) => e as int).toList(),
  );
}

Map<String, dynamic> _$GDPDataEntityToJson(GDPDataEntity instance) =>
    <String, dynamic>{
      'rates': instance.rates,
      'value_unit': instance.valueUnit,
      'values': instance.values,
      'year': instance.year,
    };
