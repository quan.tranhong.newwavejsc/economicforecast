import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'xnk_data_entity.g.dart';

@JsonSerializable()
class XNKDataEntity {
  @JsonKey(name: 'nhapkhau')
  List<XNKDataItemEntity>? nhapkhau;
  @JsonKey(name: 'xuatkhau')
  List<XNKDataItemEntity>? xuatkhau;
  @JsonKey(name: 'years')
  List<String>? years;

  XNKDataEntity({this.nhapkhau, this.xuatkhau, this.years});

  factory XNKDataEntity.fromJson(Map<String, dynamic> json) => _$XNKDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$XNKDataEntityToJson(this);
}