// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cpi_data_item_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CpiDataItemEntity _$CpiDataItemEntityFromJson(Map<String, dynamic> json) {
  return CpiDataItemEntity(
    name: json['name'] as String?,
    val: (json['val'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
  );
}

Map<String, dynamic> _$CpiDataItemEntityToJson(CpiDataItemEntity instance) =>
    <String, dynamic>{
      'name': instance.name,
      'val': instance.val,
    };
