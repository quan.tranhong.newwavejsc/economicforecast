// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unem_data_item_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UnemDataItemEntity _$UnemDataItemEntityFromJson(Map<String, dynamic> json) {
  return UnemDataItemEntity(
    indexName: json['index_name'] as String?,
    value: (json['value'] as List<dynamic>?)
        ?.map((e) => UnemValueEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$UnemDataItemEntityToJson(UnemDataItemEntity instance) =>
    <String, dynamic>{
      'index_name': instance.indexName,
      'value': instance.value,
    };
