import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'expenditure_data_entity.g.dart';

@JsonSerializable()
class ExpenditureDataEntity {
  @JsonKey(name: 'details')
  List<ExpenditureDataDetailEntity>? details;
  @JsonKey(name: 'domestic_export')
  ExpenditureDataDetailEntity? domesticExport;
  @JsonKey(name: 'foreign_export')
  ExpenditureDataDetailEntity? foreignExport;
  @JsonKey(name: 'total_export')
  ExpenditureDataDetailEntity? totalExport;
  @JsonKey(name: 'timeline')
  List<String>? timeline;

  ExpenditureDataEntity({this.details, this.domesticExport, this.foreignExport, this.totalExport, this.timeline});

  factory ExpenditureDataEntity.fromJson(Map<String, dynamic> json) => _$ExpenditureDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ExpenditureDataEntityToJson(this);
}