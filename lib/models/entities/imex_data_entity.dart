import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'imex_data_entity.g.dart';

@JsonSerializable()
class ImexDataEntity {
  @JsonKey(name: 'thuchi_data')
  List<ImexDataItemEntity>? thuchiData;
  @JsonKey(name: 'timeline')
  List<String>? timeline;

  ImexDataEntity({this.thuchiData, this.timeline});

  factory ImexDataEntity.fromJson(Map<String, dynamic> json) => _$ImexDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ImexDataEntityToJson(this);

}