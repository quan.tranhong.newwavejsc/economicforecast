// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unem_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UnemEntity _$UnemEntityFromJson(Map<String, dynamic> json) {
  return UnemEntity(
    data: json['data'] == null
        ? null
        : UnemDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UnemEntityToJson(UnemEntity instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
