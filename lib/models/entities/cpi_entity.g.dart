// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cpi_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CPIEntity _$CPIEntityFromJson(Map<String, dynamic> json) {
  return CPIEntity(
    data: json['data'] == null
        ? null
        : CPIDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CPIEntityToJson(CPIEntity instance) => <String, dynamic>{
      'data': instance.data,
    };
