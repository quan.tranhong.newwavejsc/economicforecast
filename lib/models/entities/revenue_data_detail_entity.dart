import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'revenue_data_detail_entity.g.dart';

@JsonSerializable()
class RevenueDataDetailEntity {
  @JsonKey(name: 'value')
  List<double>? value;
  @JsonKey(name: 'name')
  String? name;

  RevenueDataDetailEntity({this.value, this.name});

  factory RevenueDataDetailEntity.fromJson(Map<String, dynamic> json) => _$RevenueDataDetailEntityFromJson(json);

  Map<String, dynamic> toJson() => _$RevenueDataDetailEntityToJson(this);
}