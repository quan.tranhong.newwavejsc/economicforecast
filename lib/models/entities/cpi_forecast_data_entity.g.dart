// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cpi_forecast_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CPIForecastDataEntity _$CPIForecastDataEntityFromJson(
    Map<String, dynamic> json) {
  return CPIForecastDataEntity(
    cpi: (json['cpi'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    timeline:
        (json['timeline'] as List<dynamic>?)?.map((e) => e as String).toList(),
  )
    ..lower = (json['lower'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList()
    ..upper = (json['upper'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList();
}

Map<String, dynamic> _$CPIForecastDataEntityToJson(
        CPIForecastDataEntity instance) =>
    <String, dynamic>{
      'cpi': instance.cpi,
      'timeline': instance.timeline,
      'lower': instance.lower,
      'upper': instance.upper,
    };
