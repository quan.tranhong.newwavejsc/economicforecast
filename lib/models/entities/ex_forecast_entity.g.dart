// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ex_forecast_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExForecastEntity _$ExForecastEntityFromJson(Map<String, dynamic> json) {
  return ExForecastEntity(
    data: json['data'] == null
        ? null
        : ExForecastDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ExForecastEntityToJson(ExForecastEntity instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
