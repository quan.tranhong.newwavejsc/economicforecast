// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expenditure_data_detail_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpenditureDataDetailEntity _$ExpenditureDataDetailEntityFromJson(
    Map<String, dynamic> json) {
  return ExpenditureDataDetailEntity(
    value: (json['value'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    name: json['name'] as String?,
  );
}

Map<String, dynamic> _$ExpenditureDataDetailEntityToJson(
        ExpenditureDataDetailEntity instance) =>
    <String, dynamic>{
      'value': instance.value,
      'name': instance.name,
    };
