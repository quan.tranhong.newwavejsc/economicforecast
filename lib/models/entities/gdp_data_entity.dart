import 'package:json_annotation/json_annotation.dart';

part 'gdp_data_entity.g.dart';

@JsonSerializable()
class GDPDataEntity {
  @JsonKey(name: 'rates')
  List<double>? rates;
  @JsonKey(name: 'value_unit')
  String? valueUnit;
  @JsonKey(name: 'values')
  List<double>? values;
  @JsonKey(name: 'year')
  List<int>? year;

  GDPDataEntity({this.rates, this.valueUnit, this.values, this.year});

  factory GDPDataEntity.fromJson(Map<String, dynamic> json) => _$GDPDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$GDPDataEntityToJson(this);
}