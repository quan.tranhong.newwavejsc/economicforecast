// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'revenue_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RevenueDataEntity _$RevenueDataEntityFromJson(Map<String, dynamic> json) {
  return RevenueDataEntity(
    details: (json['details'] as List<dynamic>?)
        ?.map(
            (e) => RevenueDataDetailEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    domesticImport: json['domestic_import'] == null
        ? null
        : RevenueDataDetailEntity.fromJson(
            json['domestic_import'] as Map<String, dynamic>),
    foreignImport: json['foreign_import'] == null
        ? null
        : RevenueDataDetailEntity.fromJson(
            json['foreign_import'] as Map<String, dynamic>),
    totalImport: json['total_import'] == null
        ? null
        : RevenueDataDetailEntity.fromJson(
            json['total_import'] as Map<String, dynamic>),
    timeline:
        (json['timeline'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$RevenueDataEntityToJson(RevenueDataEntity instance) =>
    <String, dynamic>{
      'details': instance.details,
      'domestic_import': instance.domesticImport,
      'foreign_import': instance.foreignImport,
      'total_import': instance.totalImport,
      'timeline': instance.timeline,
    };
