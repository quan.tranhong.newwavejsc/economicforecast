import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'iip_forecast_data_entity.g.dart';

@JsonSerializable()
class IIPForecastDataEntity {
  @JsonKey(name: 'iip')
  List<double>? cpi;
  @JsonKey(name: 'timeline')
  List<String>? timeline;
  @JsonKey(name: 'lower')
  List<double>? lower;
  @JsonKey(name: 'upper')
  List<double>? upper;

  IIPForecastDataEntity({this.cpi, this.timeline});

  factory IIPForecastDataEntity.fromJson(Map<String, dynamic> json) => _$IIPForecastDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$IIPForecastDataEntityToJson(this);
}