// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'xnk_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

XNKEntity _$XNKEntityFromJson(Map<String, dynamic> json) {
  return XNKEntity(
    data: json['data'] == null
        ? null
        : XNKDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$XNKEntityToJson(XNKEntity instance) => <String, dynamic>{
      'data': instance.data,
    };
