import 'package:equatable/equatable.dart';
import 'chart_presenter.dart';

class ChartGroupPresenter extends Equatable {
  List<ChartPresenter>? presenters;
  int? key;
  String? name;

  ChartGroupPresenter({this.presenters, this.key, this.name});

  @override
  List<Object> get props => [this.key ?? 0, this.presenters ?? [], this.name ?? ""];

}
