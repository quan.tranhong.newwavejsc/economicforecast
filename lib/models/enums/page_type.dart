enum PageType {
  CPI,
  IIP,
  GDP,
  IMPORT,
  EXPORT,
  UNEMPLOYMENT,
  IMPORT_EXPORT
}

extension PageTypeExtension on PageType {
  // int get value {
  //   switch(this){
  //     case BankLoanType.YES:
  //       return 1;
  //     case BankLoanType.NO:
  //       return 2;
  //     default:
  //       return 0;
  //   }
  // }
}
