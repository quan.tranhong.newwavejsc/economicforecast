enum ForecastDurationType {
  SIX_MONTH,
  A_YEAR,
  NINE_MONTH,
  OVER_A_YEAR,
  ALL_YEAR,
  OVER_ALL_YEAR,
}

extension ForecastDurationTypeExtension on ForecastDurationType {
  double get value {
    switch(this){
      case ForecastDurationType.SIX_MONTH:
        return 7;
      case ForecastDurationType.A_YEAR:
        return 13;
      case ForecastDurationType.NINE_MONTH:
        return 10;
      case ForecastDurationType.OVER_A_YEAR:
        return 16;
      case ForecastDurationType.ALL_YEAR:
        return double.infinity;
      case ForecastDurationType.OVER_ALL_YEAR:
        return 10;
      default:
        return 0;
    }
  }

  double get statValue {
    switch(this){
      case ForecastDurationType.SIX_MONTH:
        return 6;
      case ForecastDurationType.A_YEAR:
        return 12;
      case ForecastDurationType.NINE_MONTH:
        return 9;
      case ForecastDurationType.OVER_A_YEAR:
        return 15;
      case ForecastDurationType.ALL_YEAR:
        return double.infinity;
      case ForecastDurationType.OVER_ALL_YEAR:
        return 9;
      default:
        return 0;
    }
  }

  String get name {
    switch(this){
      case ForecastDurationType.SIX_MONTH:
        return "6 Tháng";
      case ForecastDurationType.A_YEAR:
        return "12 Tháng";
      case ForecastDurationType.NINE_MONTH:
        return "Hiển thị biểu đồ 6 tháng gần nhất";
      case ForecastDurationType.OVER_A_YEAR:
        return "Hiển thị biểu đồ 1 năm gần nhất";
      case ForecastDurationType.ALL_YEAR:
        return "Tất cả";
      case ForecastDurationType.OVER_ALL_YEAR:
        return "Hiển thị toàn bộ biểu đồ";
      default:
        return "";
    }
  }

  String get number {
    switch(this){
      case ForecastDurationType.SIX_MONTH:
        return "6";
      case ForecastDurationType.A_YEAR:
        return "12";
      case ForecastDurationType.NINE_MONTH:
        return "6";
      case ForecastDurationType.OVER_A_YEAR:
        return "12";
      case ForecastDurationType.ALL_YEAR:
        return "All";
      case ForecastDurationType.OVER_ALL_YEAR:
        return "All";
      default:
        return "0";
    }
  }
}
