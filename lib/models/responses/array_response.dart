import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/movie_entity.dart';

class ArrayResponse<T> extends Equatable {
  final int? page;
  final int? pageSize;
  final int? totalPages;
  final List<T>? results;

  ArrayResponse({
    this.page,
    this.pageSize,
    this.totalPages,
    this.results,
  });

  ArrayResponse copyWith({
    int? page,
    int? pageSize,
    int? totalPages,
    List<T>? results,
  }) {
    if ((page == null || identical(page, this.page)) &&
        (pageSize == null || identical(pageSize, this.pageSize)) &&
        (totalPages == null || identical(totalPages, this.totalPages)) &&
        (results == null || identical(results, this.results))) {
      return this;
    }

    return new ArrayResponse(
      page: page ?? this.page,
      pageSize: pageSize ?? this.pageSize,
      totalPages: totalPages ?? this.totalPages,
      results: results ?? this.results,
    );
  }

  static T _objectToFrom<T>(json) {
    switch (T.toString()) {
      case 'MovieEntity':
        return MovieEntity.fromJson(json) as T;
      default:
        return json as T;
    }
  }

  @override
  List<Object> get props => [
        page ?? 0,
        pageSize ?? 0,
        totalPages ?? 0,
        results ?? [],
      ];
}
