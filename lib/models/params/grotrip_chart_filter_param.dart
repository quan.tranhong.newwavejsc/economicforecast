import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_group_presenter.dart';
import 'package:flutter_base/models/entities/chart_group_reg_presenter.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';

// ignore: must_be_immutable
class GrotripChartFilterParam extends Equatable {
  ChartGroupPresenter? chartPresenter;

  GrotripChartFilterParam({
    this.chartPresenter,
  });

  GrotripChartFilterParam copyWith({
    ChartGroupPresenter? chartPresenter,
  }) {
    return new GrotripChartFilterParam(
      chartPresenter: chartPresenter ?? this.chartPresenter,
    );
  }

  @override
  List<Object> get props => [
        chartPresenter ?? ChartGroupPresenter(),
      ];
}
