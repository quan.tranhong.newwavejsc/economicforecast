import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';

// ignore: must_be_immutable
class LineChartFilterParam extends Equatable {
  List<ChartPresenter>? chartPresenters;
  ChartPresenter? chartPresenter;
  List<ChartPresenter>? choosens;

  LineChartFilterParam(
      {List<ChartPresenter>? chartPresenters,
      ChartPresenter? chartPresenter,
        List<ChartPresenter>? choosens}) {
    this.chartPresenters = chartPresenters ?? [];
    this.chartPresenter = chartPresenter ?? ChartPresenter();
    this.choosens = choosens ?? [];
  }

  List<ChartPresenter>? get presenterList {
    if(choosens?.isEmpty == true) return chartPresenters;
    ChartPresenter presenter = choosens![0];
    int index = -1;
    for(int i=0; i< (chartPresenters?.length ?? 0); i++){
      if(chartPresenters?[i].key == presenter.key){
        index = i;
      }
    }
    if(index == -1) return chartPresenters;
    else {
      return [chartPresenters![index]];
    }
  }

  LineChartFilterParam copyWith(
      {List<ChartPresenter>? chartPresenters,
      ChartPresenter? chartPresenter,
      List<ChartPresenter>? choosens}) {
    return new LineChartFilterParam(
      chartPresenters: chartPresenters ?? this.chartPresenters,
      chartPresenter: chartPresenter ?? this.chartPresenter,
      choosens: choosens ?? this.choosens,
    );
  }

  @override
  List<Object> get props => [
        chartPresenters ?? [],
        chartPresenter ?? ChartPresenter(),
        choosens ?? []
      ];
}
