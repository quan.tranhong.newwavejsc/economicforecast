import 'package:dio/dio.dart';
import 'package:flutter_base/configs/app_config.dart';
import 'package:flutter_base/network/amazon_client.dart';
import 'package:flutter_base/network/api_client.dart';

import 'api_interceptors.dart';

class ApiUtil {
  static ApiClient getApiClient() {
    final dio = Dio();
    dio.options.connectTimeout = 30000;
    dio.interceptors.add(ApiInterceptors());
    final apiClient = ApiClient(dio, baseUrl: AppConfig.baseUrl);
    return apiClient;
  }

  static AmazonClient getAmazonClient() {
    final dio = Dio();
    dio.options.connectTimeout = 60000;
    dio.interceptors.add(ApiInterceptors());
    final apiClient = AmazonClient(dio, baseUrl: AppConfig.baseUrl);
    return apiClient;
  }
}
