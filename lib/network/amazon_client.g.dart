// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'amazon_client.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _AmazonClient implements AmazonClient {
  _AmazonClient(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://aic-group.bike/api/v1/dong-nai';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<void> uploadAvatar() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    await _dio.fetch<void>(_setStreamType<void>(
        Options(method: 'POST', headers: <String, dynamic>{}, extra: _extra)
            .compose(_dio.options, '/avatar',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    return null;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
