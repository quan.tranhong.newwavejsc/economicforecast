import 'package:dio/dio.dart';
import 'package:flutter_base/configs/app_config.dart';
import 'package:flutter_base/models/entities/movie_entity.dart';
import 'package:flutter_base/models/responses/array_response.dart';
import 'package:retrofit/retrofit.dart';

part 'amazon_client.g.dart';

@RestApi(baseUrl: AppConfig.baseUrl)
abstract class AmazonClient {
  factory AmazonClient(Dio dio, {String baseUrl}) = _AmazonClient;

  /// Update avatar
  @POST("/avatar")
  Future<void> uploadAvatar();
}
