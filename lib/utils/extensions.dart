import 'package:flutter_base/models/entities/chart_presenter.dart';

extension ListExtension<T> on List<T> {
  /// quanth: force sub list max 15 phần tử đối với api chưa có chức năng sublist
  List<T> forceSubList(){
    int start = 0;
    int size = 15;
    int length = this.length;
    if(length <= 15) {
      start = 0;
      size = length;
    } else {
      start = length - 15;
      size = length;
    }

    List<T> tmp = this.sublist(start, size);
    return tmp;
  }

  /// quanth: kiểm tra xem chart presenter bất kỳ có trong danh sách k?
  List<T> containChartPresenter(ChartPresenter item){
    return this.where((i) => (i as ChartPresenter).key == item.key).toList();
  }
}