import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/network/api_client.dart';

abstract class ChartRepository {
  Future<CPIEntity>? fetchCPI();
  Future<IipEntity>? fetchIIP();
  Future<GDPEntity>? fetchGDP();
  Future<XNKEntity>? fetchXNK();
  Future<ImexEntity>? fetchImEx();
  Future<UnemEntity>? fetchUnem();
  Future<CPIForecastEntity>? fetchForecastCPI();
  Future<IIPForecastEntity>? fetchForecastIIP();
  Future<RevenueEntity>? fetchRev();
  Future<ExpenditureEntity>? fetchExp();
  Future<ImForecastEntity>? fetchForecastRevenue();
  Future<ExForecastEntity>? fetchForecastExpenditure();
}

class ChartRepositoryImpl extends ChartRepository {
  ApiClient? _apiClient;

  ChartRepositoryImpl(ApiClient client) {
    _apiClient = client;
  }

  @override
  Future<CPIEntity>? fetchCPI() {
    return _apiClient?.fetchCPI(false);
  }

  @override
  Future<CPIForecastEntity>? fetchForecastCPI() {
    return _apiClient?.fetchForecastCPI(15, 0.85);
  }

  @override
  Future<IipEntity>? fetchIIP() {
    return _apiClient?.fetchIIP(15, false);
  }

  @override
  Future<IIPForecastEntity>? fetchForecastIIP() {
    return _apiClient?.fetchForecastIIP(15, 0.85);
  }

  @override
  Future<GDPEntity>? fetchGDP() {
    return _apiClient?.fetchGDP(15, false);
  }

  @override
  Future<XNKEntity>? fetchXNK() {
    return _apiClient?.fetchXNK(15, false);
  }

  @override
  Future<ImexEntity>? fetchImEx() {
    return _apiClient?.fetchImEx(15, true);
  }

  @override
  Future<UnemEntity>? fetchUnem() {
    return _apiClient?.fetchUnemployment(15, false);
  }

  @override
  Future<ExpenditureEntity>? fetchExp() {
    return _apiClient?.fetchExpenditure();
  }

  @override
  Future<RevenueEntity>? fetchRev() {
    return _apiClient?.fetchRevenue();
  }

  @override
  Future<ExForecastEntity>? fetchForecastExpenditure() {
    return _apiClient?.fetchForecastExpenditure(3, 0.85);
  }

  @override
  Future<ImForecastEntity>? fetchForecastRevenue() {
    return _apiClient?.fetchForecastRevenue(3, 0.85);
  }}
