package it.thoson.flutter_base.application

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.adjust.sdk.Adjust
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.flutter.app.FlutterApplication

class MainApplication : FlutterApplication() {

    var isLoggingOut = false
    private lateinit var mGson: Gson

    companion object {
        private lateinit var mSelf: MainApplication

        fun self(): MainApplication {
            return mSelf
        }
    }

    fun getGson(): Gson {
        return mGson
    }


    override fun onCreate() {
        super.onCreate()
        mSelf = this
        mGson = GsonBuilder().registerTypeAdapterFactory(LenientTypeAdapterFactory()).create()
    }

    // You can use this class if your app is for Android 4.0 or higher
    private class AdjustLifecycleCallbacks : ActivityLifecycleCallbacks {
        override fun onActivityResumed(activity: Activity) {
            Adjust.onResume()
        }

        override fun onActivityPaused(activity: Activity) {
            Adjust.onPause()
        }

        override fun onActivityStopped(activity: Activity) {}

        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}

        override fun onActivityDestroyed(activity: Activity) {}

        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {}

        override fun onActivityStarted(activity: Activity) {}
    }

}
