package it.thoson.flutter_base.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.DisplayMetrics
import androidx.fragment.app.FragmentActivity
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs


object Utils {
    /**
     * Get color wrapper
     *
     * @param context
     * @param id
     * @return
     */
    @Suppress("DEPRECATION")
    fun getColor(context: Context?, id: Int): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context?.getColor(id) ?: 0
        } else {
            context?.resources?.getColor(id) ?: 0
        }
    }

    /**
     * @param dps value of dps
     * @return value of pixel
     */
    fun convertDpsToPixel(context: Context?, dps: Int): Int {
        if (context == null) return 0
        val scale = context.resources.displayMetrics.density
        return (dps * scale + 0.5f).toInt()
    }

    /**
     * get Screen Size
     *
     * @param activity
     * @return
     */
    fun getScreenSize(activity: Activity?): DisplayMetrics {
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        return displayMetrics
    }

    /**
     * get Screen Size
     *
     * @param fragmentActivity
     * @return
     */
    fun getScreenSize(fragmentActivity: FragmentActivity?): DisplayMetrics {
        val displayMetrics = DisplayMetrics()
        fragmentActivity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        return displayMetrics
    }


    /**
     * convert long to datetime
     *
     * @param pattern: string format
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    fun convertLongToDate(time: Long?, pattern: String?): String {
        val date = time?.let { Date(it) }
        val format = SimpleDateFormat(pattern)
        return try {
            format.format(date)
        } catch (e: Exception) {
            ""
        }
    }

    /**
     * calculate number of day remain
     */
    fun getRemainDay(timeMillisecondRemain: Long?): Long {
        if (timeMillisecondRemain == null) return 0
        return if (timeMillisecondRemain > 86400000) {
            timeMillisecondRemain.div(86400000)
        } else {
            0
        }
    }

    /**
     * calculate number of day remain
     */
    fun getRemainMonth(dayCount: Long?): Long {
        if (dayCount == null) return 0
        return if (dayCount > 30) {
            dayCount.div(30)
        } else {
            0
        }
    }

//    /**
//     * INCLUDE DAY TO HOURS
//     * calculate full time remain with format type
//     */
//    fun getFullRemainTime(
//        timeMillisecondRemain: Long?,
//        remainTimeFormatType: Constants.RemainTimeFormatType
//    ): String {
//        var result = when (remainTimeFormatType) {
//            Constants.RemainTimeFormatType.HH_MM_SS -> "00:00:00"
//            else -> "00:00"
//        }
//        if (timeMillisecondRemain == null) return result
//
//        if (timeMillisecondRemain > 0) {
//            val second = timeMillisecondRemain / 1000
//            result = when (remainTimeFormatType) {
//                Constants.RemainTimeFormatType.HH_MM_SS ->
//                    String.format(
//                        "%02d:%02d:%02d",
//                        second / 3600,
//                        (second % 3600) / 60,
//                        (second % 60)
//                    )
//                Constants.RemainTimeFormatType.HH_MM ->
//                    String.format("%02d:%02d", second / 3600, (second % 3600) / 60)
//                Constants.RemainTimeFormatType.MM_SS ->
//                    String.format("%02d:%02d", second / 60, (second % 60))
//            }
//        }
//        return result
//    }

    /**
     * NOT COUNT DAY
     * calculate time remain with format hh:mm:ss
     */
//    fun getTimeRemain(
//        timeMillisecondRemain: Long,
//        remainTimeFormatType: Constants.RemainTimeFormatType
//    ): String {
//        var result = when (remainTimeFormatType) {
//            Constants.RemainTimeFormatType.HH_MM_SS -> "00:00:00"
//            else -> "00:00"
//        }
//
//        val remainDay = getRemainDay(timeMillisecondRemain)
//        val remainDayToMillisecond = remainDay * 24 * 60 * 60 * 1000L
//
//        if (timeMillisecondRemain - remainDayToMillisecond > 0) {
//            result = getFullRemainTime(
//                timeMillisecondRemain - remainDayToMillisecond,
//                remainTimeFormatType
//            )
//        }
//        return result
//    }

    /**
     * get text time ago from timeMillisecond
     */
//    fun getTextTimeAgo(context: Context?, timeMillisecond: Long): String {
//        if (context == null) return ""
//        val secondMillis = 1000
//        val minuteMillis = 60 * secondMillis
//        val hourMillis = 60 * minuteMillis
//        val dayMillis = 24 * hourMillis
//
//        val now = Calendar.getInstance().timeInMillis
//        if (timeMillisecond > now || timeMillisecond <= 0) {
//            return ""
//        }
//
//        val diff = now - timeMillisecond
//        return when {
//            diff < minuteMillis -> context.getString(R.string.time_ago_just_now)
//            diff < 2 * minuteMillis -> context.getString(R.string.time_ago_minute)
//            diff < 50 * minuteMillis -> context.getString(
//                R.string.time_ago_minutes,
//                (diff / minuteMillis).toString()
//            )
//            diff < 90 * minuteMillis -> context.getString(R.string.time_ago_hour)
//            diff < 24 * hourMillis -> context.getString(
//                R.string.time_ago_hours,
//                (diff / hourMillis).toString()
//            )
//            diff < 48 * hourMillis -> context.getString(R.string.yesterday).toLowerCase()
//            else -> context.getString(R.string.time_ago_days, (diff / dayMillis).toString())
//        }
//    }

    /**
     * QuanTH: get text time ago from timeMillisecond only from clan
     */
//    fun getClanMessageTextTimeAgo(context: Context?, timeMillisecond: Long): String {
//        if (context == null) return ""
//        val secondMillis = 1000
//        val minuteMillis = 60 * secondMillis
//        val hourMillis = 60 * minuteMillis
//        val dayMillis = 24 * hourMillis
//
//        val now = Calendar.getInstance().timeInMillis
//        if (timeMillisecond > now || timeMillisecond <= 0) {
//            if (timeMillisecond > now)
//                context.getString(R.string.time_ago_just_now)
//            else
//                return ""
//        }
//
//        val diff = now - timeMillisecond
//        return when {
//            diff <= minuteMillis -> context.getString(R.string.time_ago_just_now)
//            diff < 2 * minuteMillis -> context.getString(R.string.time_ago_minute)
//            diff < 50 * minuteMillis -> context.getString(
//                R.string.time_ago_minutes,
//                (diff / minuteMillis).toString()
//            )
//            diff < 90 * minuteMillis -> context.getString(R.string.time_ago_hour)
//            diff < 24 * hourMillis -> context.getString(
//                R.string.time_ago_hours,
//                (diff / hourMillis).toString()
//            )
//            diff < 48 * hourMillis -> context.getString(R.string.yesterday).toLowerCase()
//            else -> context.getString(R.string.time_ago_days, (diff / dayMillis).toString())
//        }
//    }

    /**
     * get text time ago from timeMillisecond
     */
//    fun getTimeExprired(context: Context?, timeMillisecond: Long): String {
//        if (context == null) return ""
//        val secondMillis = 1000
//        val minuteMillis = 60 * secondMillis
//        val hourMillis = 60 * minuteMillis
//        val dayMillis = 24 * hourMillis
//
//        val now = Calendar.getInstance().timeInMillis
//        if (timeMillisecond < now || timeMillisecond <= 0) {
//            return "Expired"
//        }
//
//        val diff = timeMillisecond - now
//        return when {
//            diff < minuteMillis -> context.getString(R.string.expired_time_ago_just_now)
//            diff < 2 * minuteMillis -> context.getString(R.string.expired_time_ago_minute)
//            diff < 50 * minuteMillis -> context.getString(
//                R.string.expired_time_ago_minutes,
//                (diff / minuteMillis).toString()
//            )
//            diff < 90 * minuteMillis -> context.getString(R.string.expired_time_ago_hour).toLowerCase()
//            diff < 24 * hourMillis -> context.getString(
//                R.string.expired_time_ago_hours,
//                (diff / hourMillis).toString()
//            )
//            diff < 48 * hourMillis -> context.getString(R.string.expired_time_tomorrow).toLowerCase()
//            else -> context.getString(R.string.expired_time_ago_days, (diff / dayMillis).toString())
//        }
//    }

    /**
     * convert TimeZone to String
     * Ex: GMT+7:00
     * Ex: GMT-8:00
     * Ex: GMT0:00
     */
    fun displayTimeZone(tz: TimeZone): String {
        val hours = TimeUnit.MILLISECONDS.toHours(tz.rawOffset.toLong())
        var minutes =
            TimeUnit.MILLISECONDS.toMinutes(tz.rawOffset.toLong()) - TimeUnit.HOURS.toMinutes(hours)
        // avoid -4:-30 issue
        minutes = abs(minutes)

        return if (hours > 0) {
            String.format("GMT+%d:%02d", hours, minutes)
        } else {
            String.format("GMT%d:%02d", hours, minutes)
        }
    }

    /**
     * sub file name from Url
     *
     * @param url
     * @return
     */
    fun subFileName(url: String): String? {
        try {
            val slashIndex = url.lastIndexOf('/')
            val dotIndex =
                if (slashIndex != -1) url.substring(
                    slashIndex,
                    url.length
                ).lastIndexOf('.') else url.lastIndexOf('.')
            return if (slashIndex != -1 && dotIndex != -1) {
                val fileName = url.substring(slashIndex + 1, url.length)
                fileName.substring(0, fileName.lastIndexOf('.'))
            } else if (slashIndex == -1 && dotIndex != -1) {
                url.substring(0, dotIndex)
            } else if (slashIndex != -1) {
                url.substring(slashIndex + 1, url.length)
            } else {
                url
            }
        } catch (e: Exception) {
            return null
        }
    }

    /**
     * sub file extension from Url
     *
     * @param url
     * @return
     */
    fun subFileExtension(url: String): String? {
        return try {
            val dotIndex = url.lastIndexOf('.')
            if (dotIndex != -1) {
                url.substring(dotIndex + 1, url.length)
            } else {
                null
            }
        } catch (e: Exception) {
            null
        }
    }

    fun rotateBitmap(bitmap: Bitmap, orientation: Int): Bitmap? {

        val matrix = Matrix()
        when (orientation) {
            ExifInterface.ORIENTATION_NORMAL -> return bitmap
            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> matrix.setScale(-1f, 1f)
            ExifInterface.ORIENTATION_ROTATE_180 -> matrix.setRotate(180f)
            ExifInterface.ORIENTATION_FLIP_VERTICAL -> {
                matrix.setRotate(180f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_TRANSPOSE -> {
                matrix.setRotate(90f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_ROTATE_90 -> matrix.setRotate(90f)
            ExifInterface.ORIENTATION_TRANSVERSE -> {
                matrix.setRotate(-90f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_ROTATE_270 -> matrix.setRotate(-90f)
            else -> return bitmap
        }
        return try {
            val bmRotated =
                Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            bitmap.recycle()
            bmRotated
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
            null
        }

    }

    fun getImageUri(context: Context?, myBitmap: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(context?.contentResolver, myBitmap, "Avatar", null)
        return Uri.parse(path)
    }

    /**
     * QuanTH: get time as format: "yyyy-MM-dd HH:mm:ss"
     */
    fun getTimestampAsDateTime(timeMillisecond: Double): String {
        var result: String?
        val cal = Calendar.getInstance()
        cal.timeInMillis = timeMillisecond.toLong()
        val yy = cal.get(Calendar.YEAR)
        val mm = cal.get(Calendar.MONTH)
        val dd = cal.get(Calendar.DAY_OF_MONTH)
        val date =
            yy.toString() + "-" +
                    (if ((mm + 1) < 10) ("0" + (mm + 1).toString()) else (mm + 1).toString()) + "-" +
                    (if (dd < 10) ("0$dd") else dd.toString())
        val hour = cal.get(Calendar.HOUR_OF_DAY)
        val minute = cal.get(Calendar.MINUTE)
        val second = cal.get(Calendar.SECOND)
        val time = (if (hour < 10) ("0$hour") else hour.toString()) + ":" +
                (if (minute < 10) ("0$minute") else minute.toString()) + ":" +
                (if (second < 10) ("0$second") else second.toString())
        result = "$date $time"
        return result
    }

    /**
     * QuanTH: get time as second
     */
    fun getTimestampAsSecond(timeMillisecond: Double): String {
        var result: String?
        val cal = Calendar.getInstance()
        cal.timeInMillis = timeMillisecond.toLong()
        val second = cal.get(Calendar.SECOND)
        result = (if (second < 10) ("0$second") else second.toString())
        return result
    }

    fun getLocalizedResources(desiredLocale: Locale, context: Context): Resources {
        var conf = context?.getResources().getConfiguration()
        conf = Configuration(conf)
        conf.setLocale(desiredLocale)
        val localizedContext = context?.createConfigurationContext(conf)
        return localizedContext.getResources()
    }

    fun changeTimezoneOfDate(date: Date, fromTZ: TimeZone, toTZ: TimeZone): Calendar {
        // TimeZone.getTimeZone("GMT")
        val calendar = Calendar.getInstance()
        calendar.time = date
        val millis = calendar.timeInMillis
        val fromOffset = fromTZ.getOffset(millis)
        val toOffset = toTZ.getOffset(millis)
        val convertedTime = millis - (fromOffset - toOffset)
        val c = Calendar.getInstance()
        c.timeInMillis = convertedTime
        return c
    }

//    fun recordLogs(msg: String?, errorName: String?){
//        Log.e("quanth", "error of function ${msg}", Exception("${msg}"))
//        Crashlytics.logException(Exception("${errorName}"))
//        Crashlytics.getInstance().core.logException(Exception("${errorName}"))
//    }

    fun isMokeupData(): Boolean{
        return false
    }

    // check app installed

    fun isPackageInstalled(packageName: String?, packageManager: PackageManager?): Boolean {
        return try {
            packageName?.let { packageManager?.getPackageInfo(it, 0) }
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    fun openOtherApp(context: Context?,packageName:String,packageManager: PackageManager?){
        val launchIntent =
            packageManager?.getLaunchIntentForPackage(packageName)
        if (launchIntent != null) {
            launchIntent.apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, "This is my text to send.")
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(launchIntent, null)
            context?.startActivity(shareIntent) //null pointer check in case package name was not found
        }
    }

}