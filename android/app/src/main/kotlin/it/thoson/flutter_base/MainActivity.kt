package it.thoson.flutter_base

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.UriMatcher
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Toast
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import it.thoson.flutter_base.helpers.SharedPreferencesHelper
import it.thoson.flutter_base.utils.Utils

class MainActivity : FlutterActivity() {

    companion object {
        private const val CHANNEL = "com.aic.sso"
        private const val KEY_CHECK_TOKEN = "sso_token"
        private const val KEY_OPEN_SSO_CENTER = "open_sso_center"
        private const val KEY_LOGOUT = "logout"
        private const val AUTHORITY = "com.example.centersso"
        private const val BASE_PATH = "tokens"
        val CONTENT_URI = Uri.parse("content://$AUTHORITY/$BASE_PATH")
        var token = ""
        var oldParterToken = ""

        val packageSSO = "com.example.centersso.dev"
    }

    // Constant to identify the requested operation
    private val TOKENS = 1
    private val TOKEN_ID = 2

    private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH)

    // channel
    private var channel: MethodChannel? = null
    private var runningTask: LongOperation? = null

    init {
        uriMatcher.addURI(AUTHORITY, BASE_PATH, TOKENS)
        uriMatcher.addURI(AUTHORITY, "$BASE_PATH/#", TOKEN_ID)
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        showLog(funcName = "configureFlutterEngine")

        // đăng ký channel trả token về cho flutter
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        setupChannel()
    }

    fun showLog(funcName: String){
        try {
            val data = intent.data
            val appID = data?.getQueryParameter("data").toString()
            val callback = data?.getQueryParameter("callback")?.toInt() ?:0
            Toast.makeText(context, "${funcName} appID= ${appID}, callback= ${callback}", Toast.LENGTH_LONG).show()
        } catch (e:Exception){

        }
    }

    override fun getMetaData(): Bundle? {
        return super.getMetaData()
        showLog(funcName = "getMetaData")
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        try {
            val data = intent.data
            val appID = data?.getQueryParameter("data").toString()
            val callback = data?.getQueryParameter("callback")?.toInt() ?:0
            if(appID.trim() == "sso_center" && callback == 1){
                runningTask = flutterEngine?.let {
                    LongOperation(
                        flutterEngine = it,
                        contentResolver = contentResolver,
                        context = applicationContext
                    )
                }
                runningTask?.execute()
            }
            Toast.makeText(context, "onNewIntent appID= ${appID}, callback= ${callback}", Toast.LENGTH_LONG).show()
        } catch (e:Exception){

        }
    }

    private fun setupChannel() {
        channel = MethodChannel(flutterEngine?.dartExecutor?.binaryMessenger, CHANNEL)
        channel?.setMethodCallHandler { call: MethodCall, _: MethodChannel.Result? ->
            if (call.method == KEY_CHECK_TOKEN) {
                runningTask = flutterEngine?.let {
                    LongOperation(
                        flutterEngine = it,
                        contentResolver = contentResolver,
                        context = applicationContext
                    )
                }
                runningTask?.execute()
            } else if (call.method == KEY_OPEN_SSO_CENTER) {
                gotoLoginScreen()
            } else if (call.method == KEY_LOGOUT) {
                logout()
            }
        }
    }

    private fun gotoLoginScreen() {
        val pm = activity?.packageManager
        // Nếu đã cài app sso center r thì mở app đó lên
        if (Utils.isPackageInstalled(packageSSO, pm)){
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("centersso://aic?data=dubao8cksiemcps&callback=1") // dubao8cksiemcps
            startActivity(intent)
        }else {
            // go to màn hình login của flutter
        }
    }

    private fun logout() {
        contentResolver?.delete(CONTENT_URI, null, null)
//        gotoLoginScreen()
    }

    private class LongOperation(
        flutterEngine: FlutterEngine,
        contentResolver: ContentResolver,
        context: Context
    ) :
        AsyncTask<Void, String?, String?>() {
        var contentResolver: ContentResolver? = null
        var context: Context? = null
        var flutterEngine: FlutterEngine? = null

        init {
            this.context = context
            this.contentResolver = contentResolver
            this.flutterEngine = flutterEngine
        }

        override fun onProgressUpdate(vararg values: String?) {
            super.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            // You might want to change "executed" for the returned string
            // passed into onPostExecute(), but that is up to you
            if (result?.trim()?.isNotEmpty() == true) {
                Toast.makeText(context, "token is not empty", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(context, "token is empty, go to login", Toast.LENGTH_LONG).show()
            }

            MethodChannel(
                flutterEngine?.dartExecutor?.binaryMessenger,
                CHANNEL
            ).invokeMethod("_retrieveSSOToken", result)
        }

        override fun doInBackground(vararg params: Void): String? {
            return getTokenFromParterProvider()
        }

        private fun getTokenFromParterProvider(): String {
            var token = ""
            try {
                val cursor = contentResolver?.query(
                    CONTENT_URI,
                    null,
                    null,
                    null,
                    null
                )
                if (cursor != null) {
                    if (!cursor.moveToFirst()) {
                        //Toast.makeText(this, "no data yet", Toast.LENGTH_SHORT).show()
                    } else {
                        token = cursor.getString(cursor.getColumnIndex("tokenValue"))
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return token
        }
    }

    private fun checkAuth() {
        // get token from partner provider
        token = getTokenFromParterProvider()

        SharedPreferencesHelper.instance.put(
            SharedPreferencesHelper.AUTH_TOKEN,
            token
        )

        oldParterToken = token
        if (token.trim().isNotEmpty()) {
            Toast.makeText(applicationContext, "token is not empty", Toast.LENGTH_LONG).show()
//            interactor?.getUserInfo()
        } else {
            Toast.makeText(applicationContext, "token is empty, go to login", Toast.LENGTH_LONG)
                .show()
//            router?.gotoLoginScreen()
        }
    }

    fun getTokenFromParterProvider(): String {
        var token: String = ""
        try {
            val cursor = contentResolver.query(
                CONTENT_URI,
                null,
                null,
                null,
                null
            )
            if (cursor != null) {
                if (!cursor.moveToFirst()) {
                    //Toast.makeText(this, "no data yet", Toast.LENGTH_SHORT).show()
                } else {
                    token = cursor.getString(cursor.getColumnIndex("tokenValue"))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return token
    }

    override fun onDestroy() {
        super.onDestroy()
        // Cancel running task(s) to avoid memory leaks
        runningTask?.cancel(true)
    }
}
